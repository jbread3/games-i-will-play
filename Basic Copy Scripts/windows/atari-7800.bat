mkdir atari-7800
copy "Asteroids (USA).zip" atari-7800\.
copy "Centipede (USA).zip" atari-7800\.
copy "Choplifter! (USA).zip" atari-7800\.
copy "Dig Dug (USA).zip" atari-7800\.
copy "Donkey Kong (USA).zip" atari-7800\.
copy "Donkey Kong Junior (USA).zip" atari-7800\.
copy "Galaga (USA).zip" atari-7800\.
copy "Joust (USA).zip" atari-7800\.
copy "Mario Bros. (USA).zip" atari-7800\.
copy "Ms. Pac-Man (USA).zip" atari-7800\.
copy "Pole Position II (USA).zip" atari-7800\.
copy "Robotron - 2084 (USA).zip" atari-7800\.