mkdir gameboy-advance
copy "Advance Wars (USA).zip" gameboy-advance\.
copy "Advance Wars 2 - Black Hole Rising (USA, Australia).zip" gameboy-advance\.
copy "Castlevania - Aria of Sorrow (USA).zip" gameboy-advance\.
copy "Castlevania - Circle of the Moon (USA).zip" gameboy-advance\.
copy "Final Fantasy I & II - Dawn of Souls (USA, Australia).zip" gameboy-advance\.
copy "Final Fantasy IV Advance (USA, Australia).zip" gameboy-advance\.
copy "Final Fantasy Tactics Advance (USA, Australia).zip" gameboy-advance\.
copy "Final Fantasy V Advance (USA).zip" gameboy-advance\.
copy "Final Fantasy VI Advance (USA).zip" gameboy-advance\.
copy "Final Fight One (USA).zip" gameboy-advance\.
copy "Fire Emblem (USA, Australia).zip" gameboy-advance\.
copy "Fire Emblem - The Sacred Stones (USA, Australia).zip" gameboy-advance\.
copy "Grand Theft Auto Advance (USA).zip" gameboy-advance\.
copy "Klonoa - Empire of Dreams (USA).zip" gameboy-advance\.
copy "Klonoa 2 - Dream Champ Tournament (USA).zip" gameboy-advance\.
copy "Legend of Spyro, The - A New Beginning (USA).zip" gameboy-advance\.
copy "Legend of Zelda, The - A Link to the Past & Four Swords (USA, Australia).zip" gameboy-advance\.
copy "Legend of Zelda, The - The Minish Cap (USA).zip" gameboy-advance\.
copy "Mario & Luigi - Superstar Saga (USA, Australia).zip" gameboy-advance\.
copy "Mario Golf - Advance Tour (USA).zip" gameboy-advance\.
copy "Mario Kart - Super Circuit (USA).zip" gameboy-advance\.
copy "Mario Pinball Land (USA, Australia).zip" gameboy-advance\.
copy "Mario Tennis - Power Tour (USA, Australia) (En,Fr,De,Es,It).zip" gameboy-advance\.
copy "Mario vs. Donkey Kong (USA, Australia).zip" gameboy-advance\.
copy "Mega Man & Bass (USA).zip" gameboy-advance\.
copy "Mega Man Zero 2 (USA).zip" gameboy-advance\.
copy "Mega Man Zero 3 (USA).zip" gameboy-advance\.
copy "Mega Man Zero 4 (USA).zip" gameboy-advance\.
copy "Metal Slug Advance (USA).zip" gameboy-advance\.
copy "Metroid - Zero Mission (USA).zip" gameboy-advance\.
copy "Metroid Fusion (USA, Australia).zip" gameboy-advance\.
copy "Teenage Mutant Ninja Turtles (USA).zip" gameboy-advance\.
copy "Teenage Mutant Ninja Turtles 2 - Battle Nexus (USA).zip" gameboy-advance\.
copy "Tony Hawk's Pro Skater 2 (USA, Europe).zip" gameboy-advance\.
copy "Tony Hawk's Pro Skater 3 (USA, Europe).zip" gameboy-advance\.
copy "Tony Hawk's Pro Skater 4 (USA, Europe).zip" gameboy-advance\.
copy "Wario Land 4 (USA, Europe).zip" gameboy-advance\.