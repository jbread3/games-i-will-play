mkdir sega-mastersystem
copy "Alex Kidd in Miracle World (USA, Europe).zip" sega-mastersystem\.
copy "Astro Warrior (Japan, USA).zip" sega-mastersystem\.
copy "Castle of Illusion Starring Mickey Mouse (USA).zip" sega-mastersystem\.
copy "Choplifter (USA, Europe).zip" sega-mastersystem\.
copy "Double Dragon (World).zip" sega-mastersystem\.
copy "Fantasy Zone (World) (Rev 1) (Beta).zip" sega-mastersystem\.
copy "Fantasy Zone II - The Tears of Opa-Opa (USA, Europe).zip" sega-mastersystem\.
copy "Golden Axe (USA, Europe).zip" sega-mastersystem\.
copy "Golden Axe Warrior (USA, Europe).zip" sega-mastersystem\.
copy "Golvellius (USA, Europe).zip" sega-mastersystem\.
copy "Kenseiden (USA, Europe).zip" sega-mastersystem\.
copy "Out Run (World).zip" sega-mastersystem\.
copy "Phantasy Star (USA, Europe) (Rev 2).zip" sega-mastersystem\.
copy "Psycho Fox (USA, Europe).zip" sega-mastersystem\.
copy "R-Type (World).zip" sega-mastersystem\.
copy "Sonic The Hedgehog (USA, Europe).zip" sega-mastersystem\.
copy "Space Harrier (Japan, USA).zip" sega-mastersystem\.
copy "Wonder Boy (USA, Europe).zip" sega-mastersystem\.
copy "Wonder Boy III - The Dragon's Trap (USA, Europe).zip" sega-mastersystem\.