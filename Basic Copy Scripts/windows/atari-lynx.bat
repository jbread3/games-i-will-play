mkdir atari-lynx
copy "Blue Lightning (USA, Europe).zip" atari-lynx\.
copy "California Games (USA, Europe).zip" atari-lynx\.
copy "Checkered Flag (USA, Europe).zip" atari-lynx\.
copy "Chip's Challenge (USA, Europe).zip" atari-lynx\.
copy "Electrocop (USA, Europe).zip" atari-lynx\.
copy "Klax (USA, Europe).zip" atari-lynx\.
copy "Raiden (USA) (v3.0).zip" atari-lynx\.
copy "Rampage (USA, Europe).zip" atari-lynx\.
copy "Rampart (USA, Europe).zip" atari-lynx\.
copy "Robotron 2084 (USA, Europe).zip" atari-lynx\.
copy "Rygar - Legendary Warrior (USA, Europe).zip" atari-lynx\.
copy "S.T.U.N. Runner (USA, Europe).zip" atari-lynx\.
copy "Todd's Adventures in Slime World (USA, Europe).zip" atari-lynx\.
copy "Warbirds (USA, Europe).zip" atari-lynx\.