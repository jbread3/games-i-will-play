mkdir sega-32x
copy "After Burner Complete ~ After Burner (Japan, USA).zip" sega-32x\.
copy "Blackthorne (USA).zip" sega-32x\.
copy "Cosmic Carnage (Europe).zip" sega-32x\.
copy "Doom (Japan, USA).zip" sega-32x\.
copy "Knuckles' Chaotix (Europe).zip" sega-32x\.
copy "Kolibri (USA, Europe).zip" sega-32x\.
copy "Metal Head (Japan, USA) (En,Ja).zip" sega-32x\.
copy "Mortal Kombat II (Japan, USA).zip" sega-32x\.
copy "NBA Jam - Tournament Edition (World).zip" sega-32x\.
copy "Shadow Squadron ~ Stellar Assault (USA, Europe).zip" sega-32x\.
copy "Space Harrier (Japan, USA).zip" sega-32x\.
copy "Star Wars Arcade (USA).zip" sega-32x\.
copy "Tempo (Japan, USA).zip" sega-32x\.
copy "Virtua Fighter (Japan, USA).zip" sega-32x\.
copy "Virtua Racing Deluxe (USA).zip" sega-32x\.
copy "WWF Raw (World).zip" sega-32x\.
copy "WWF WrestleMania - The Arcade Game (USA).zip" sega-32x\.
