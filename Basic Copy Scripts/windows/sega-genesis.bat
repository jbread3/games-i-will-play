mkdir sega-genesis
copy "Addams Family, The (USA, Europe).zip" sega-genesis\.
copy "Adventures of Batman & Robin, The (USA).zip" sega-genesis\.
copy "Aerobiz (USA).zip" sega-genesis\.
copy "After Burner II (USA, Europe).zip" sega-genesis\.
copy "Aladdin (USA).zip" sega-genesis\.
copy "Alex Kidd in the Enchanted Castle (USA).zip" sega-genesis\.
copy "Altered Beast (USA, Europe).zip" sega-genesis\.
copy "Atomic Runner (USA).zip" sega-genesis\.
copy "Batman - The Video Game (USA).zip" sega-genesis\.
copy "Batman Returns (World).zip" sega-genesis\.
copy "BattleTech - A Game of Armored Combat (USA).zip" sega-genesis\.
copy "Battletoads (World).zip" sega-genesis\.
copy "Battletoads-Double Dragon (USA).zip" sega-genesis\.
copy "Beast Wrestler (USA).zip" sega-genesis\.
copy "Beyond Oasis (USA).zip" sega-genesis\.
copy "Bio Hazard Battle (USA, Europe).zip
copy "Blades of Vengeance (USA, Europe).zip" sega-genesis\.
copy "Blaster Master 2 (USA).zip" sega-genesis\.
copy "Bonanza Bros. (World) (Rev B).zip" sega-genesis\.
copy "Boogerman - A Pick and Flick Adventure (USA).zip" sega-genesis\.
copy "Bubble and Squeak (USA).zip" sega-genesis\.
copy "Bubsy in - Claws Encounters of the Furred Kind (USA, Europe).zip" sega-genesis\.
copy "Buck Rogers - Countdown to Doomsday (USA, Europe).zip" sega-genesis\.
copy "Burning Force (USA).zip" sega-genesis\.
copy "Cadash (USA, Korea).zip" sega-genesis\.
copy "California Games (USA, Europe).zip" sega-genesis\.
copy "Castle of Illusion Starring Mickey Mouse (USA, Europe).zip" sega-genesis\.
copy "Castlevania - Bloodlines (USA).zip" sega-genesis\.
copy "Championship Pro-Am (USA).zip" sega-genesis\.
copy "Columns III (USA).zip" sega-genesis\.
copy "Combat Cars (USA, Europe).zip" sega-genesis\.
copy "Comix Zone (USA).zip" sega-genesis\.
copy "Contra - Hard Corps (USA, Korea).zip" sega-genesis\.
copy "Cool Spot (USA).zip" sega-genesis\.
copy "Crack Down (USA).zip" sega-genesis\.
copy "Cyborg Justice (USA, Europe).zip" sega-genesis\.
copy "DEcapAttack (USA, Europe).zip" sega-genesis\.
copy "Demolition Man (USA, Europe).zip" sega-genesis\.
copy "Desert Demolition Starring Road Runner and Wile E. Coyote (USA, Europe).zip" sega-genesis\.
copy "Desert Strike - Return to the Gulf (USA, Europe).zip" sega-genesis\.
copy "Dick Tracy (World).zip" sega-genesis\.
copy "Doom Troopers (USA).zip" sega-genesis\.
copy "Double Dragon 3 - The Arcade Game (USA, Europe).zip" sega-genesis\.
copy "Dr. Robotnik's Mean Bean Machine (USA).zip" sega-genesis\.
copy "Dragon's Fury (USA, Europe).zip" sega-genesis\.
copy "Duke Nukem 3D (Brazil).zip" sega-genesis\.
copy "Dynamite Headdy (USA, Europe).zip" sega-genesis\.
copy "Earthworm Jim (USA).zip" sega-genesis\.
copy "Earthworm Jim 2 (USA).zip" sega-genesis\.
copy "Ecco - The Tides of Time (USA).zip" sega-genesis\.
copy "Ecco the Dolphin (USA, Europe).zip" sega-genesis\.
copy "Eternal Champions (USA).zip" sega-genesis\.
copy "Ex-Mutants (USA, Europe).zip" sega-genesis\.
copy "Faery Tale Adventure, The (USA, Europe).zip" sega-genesis\.
copy "Fantastic Dizzy (USA, Europe) (En,Fr,De,Es,It).zip" sega-genesis\.
copy "Fatal Fury 2 (USA, Korea).zip" sega-genesis\.
copy "FIFA Soccer 95 (USA, Europe) (En,Fr,De,Es).zip" sega-genesis\.
copy "Fighting Masters (USA).zip" sega-genesis\.
copy "Fire Shark (USA).zip" sega-genesis\.
copy "Flicky (USA, Europe).zip" sega-genesis\.
copy "Frogger (USA).zip" sega-genesis\.
copy "Gain Ground (World) (Rev A).zip" sega-genesis\.
copy "Galaxy Force II (World) (Rev B).zip" sega-genesis\.
copy "Gargoyles (USA).zip" sega-genesis\.
copy "Gauntlet IV (USA, Europe) (En,Ja) (September 1993).zip" sega-genesis\.
copy "Gemfire (USA).zip" sega-genesis\.
copy "General Chaos (USA, Europe).zip" sega-genesis\.
copy "Ghostbusters (World) (v1.1).zip" sega-genesis\.
copy "Ghouls'n Ghosts (USA, Europe) (Rev A).zip" sega-genesis\.
copy "Golden Axe (World) (v1.1).zip" sega-genesis\.
copy "Golden Axe II (World).zip" sega-genesis\.
copy "Great Circus Mystery Starring Mickey & Minnie, The (USA).zip" sega-genesis\.
copy "Grind Stormer (USA).zip" sega-genesis\.
copy "Gunstar Heroes (USA).zip" sega-genesis\.
copy "Haunting Starring Polterguy (USA, Europe).zip" sega-genesis\.
copy "Hellfire (USA).zip" sega-genesis\.
copy "Herzog Zwei (USA, Europe).zip" sega-genesis\.
copy "High Seas Havoc (USA).zip" sega-genesis\.
copy "Immortal, The (USA, Europe).zip" sega-genesis\.
copy "James Pond - Underwater Agent (USA, Europe).zip" sega-genesis\.
copy "James Pond 3 (USA, Europe).zip" sega-genesis\.
copy "James Pond II - Codename - Robocod (USA, Europe).zip" sega-genesis\.
copy "Jungle Book, The (USA).zip" sega-genesis\.
copy "Jungle Strike (USA, Europe).zip" sega-genesis\.
copy "Jurassic Park (USA).zip" sega-genesis\.
copy "Jurassic Park - Rampage Edition (USA, Europe).zip" sega-genesis\.
copy "Kid Chameleon (USA, Europe).zip" sega-genesis\.
copy "King of the Monsters (USA).zip" sega-genesis\.
copy "King of the Monsters 2 (USA).zip" sega-genesis\.
copy "King's Bounty - The Conqueror's Quest (USA, Europe).zip" sega-genesis\.
copy "Klax (USA, Europe).zip" sega-genesis\.
copy "Landstalker (USA).zip" sega-genesis\.
copy "Last Battle (USA, Europe).zip" sega-genesis\.
copy "Light Crusader (USA).zip" sega-genesis\.
copy "Lion King, The (World).zip" sega-genesis\.
copy "Lost Vikings, The (USA).zip" sega-genesis\.
copy "Lotus Turbo Challenge (USA, Europe).zip" sega-genesis\.
copy "Madden NFL 95 (USA, Europe).zip" sega-genesis\.
copy "Marvel Land (USA).zip" sega-genesis\.
copy "Master of Monsters (USA).zip" sega-genesis\.
copy "Mazin Saga - Mutant Fighter (USA).zip" sega-genesis\.
copy "Mega Bomberman (USA).zip" sega-genesis\.
copy "Mega Turrican (USA).zip" sega-genesis\.
copy "Mickey Mania - The Timeless Adventures of Mickey Mouse (USA).zip" sega-genesis\.
copy "Micro Machines (USA, Europe) (Alt 1).zip" sega-genesis\.
copy "Mighty Morphin Power Rangers (USA).zip" sega-genesis\.
copy "Mighty Morphin Power Rangers - The Movie (USA).zip" sega-genesis\.
copy "Mortal Kombat (World).zip" sega-genesis\.
copy "Mortal Kombat 3 (USA).zip" sega-genesis\.
copy "Mortal Kombat II (World).zip" sega-genesis\.
copy "Ms. Pac-Man (USA, Europe).zip" sega-genesis\.
copy "Mutant League Football (USA, Europe).zip" sega-genesis\.
copy "Mutant League Hockey (USA, Europe).zip" sega-genesis\.
copy "NBA Jam - Tournament Edition (World).zip" sega-genesis\.
copy "NBA Live 95 (USA, Europe).zip" sega-genesis\.
copy "NHL '94 (USA, Europe).zip" sega-genesis\.
copy "Outlander (USA).zip" sega-genesis\.
copy "OutRun (USA, Europe).zip" sega-genesis\.
copy "OutRun 2019 (USA).zip" sega-genesis\.
copy "Pac-Attack (USA).zip" sega-genesis\.
copy "Paperboy (USA, Europe).zip" sega-genesis\.
copy "Phantasy Star II (USA, Europe) (Rev A).zip" sega-genesis\.
copy "Pirates! Gold (USA).zip" sega-genesis\.
copy "Populous (USA).zip" sega-genesis\.
copy "Prince of Persia (USA).zip" sega-genesis\.
copy "Puggsy (USA).zip" sega-genesis\.
copy "Punisher, The (USA).zip" sega-genesis\.
copy "QuackShot Starring Donald Duck ~ QuackShot - I Love Donald Duck - Guruzia Ou no Hihou (World) (v1.1).zip" sega-genesis\.
copy "Rambo III (World) (v1.1).zip" sega-genesis\.
copy "Ren & Stimpy Show Presents, The - Stimpy's Invention (USA).zip" sega-genesis\.
copy "Revenge of Shinobi, The (USA, Europe).zip" sega-genesis\.
copy "Rings of Power (USA, Europe).zip" sega-genesis\.
copy "Ristar (USA, Europe) (September 1994).zip" sega-genesis\.
copy "Road Rash (USA, Europe).zip" sega-genesis\.
copy "Road Rash 3 (USA, Europe).zip" sega-genesis\.
copy "Road Rash II (USA, Europe) (v1.2).zip" sega-genesis\.
copy "RoadBlasters (USA).zip" sega-genesis\.
copy "RoboCop versus The Terminator (USA).zip" sega-genesis\.
copy "Rock n' Roll Racing (USA).zip" sega-genesis\.
copy "Rocket Knight Adventures (USA).zip" sega-genesis\.
copy "Samurai Shodown (USA).zip" sega-genesis\.
copy "Shadow Dancer - The Secret of Shinobi (World).zip" sega-genesis\.
copy "Shadowrun (USA).zip" sega-genesis\.
copy "Shaq-Fu (USA, Europe).zip" sega-genesis\.
copy "Shining Force (USA).zip" sega-genesis\.
copy "Shining Force II (USA).zip" sega-genesis\.
copy "Shinobi III - Return of the Ninja Master (USA).zip" sega-genesis\.
copy "Side Pocket (USA).zip" sega-genesis\.
copy "Skeleton Krew (USA).zip" sega-genesis\.
copy "Sonic & Knuckles (World).zip" sega-genesis\.
copy "Sonic 3D Blast ~ Sonic 3D Flickies' Island (USA, Europe).zip" sega-genesis\.
copy "Sonic The Hedgehog (USA, Europe).zip" sega-genesis\.
copy "Sonic The Hedgehog 2 (World) (Rev A).zip" sega-genesis\.
copy "Sonic The Hedgehog 3 (USA).zip" sega-genesis\.
copy "Sonic The Hedgehog Spinball (USA) (Alt 1).zip" sega-genesis\.
copy "Sparkster (USA).zip" sega-genesis\.
copy "Speedball 2 - Brutal Deluxe (USA).zip" sega-genesis\.
copy "Splatterhouse 2 (USA).zip" sega-genesis\.
copy "Splatterhouse 3 (USA).zip" sega-genesis\.
copy "Star Control (USA) (Unl).zip" sega-genesis\.
copy "Starflight (USA, Europe) (v1.1).zip" sega-genesis\.
copy "Steel Empire (USA).zip" sega-genesis\.
copy "Street Fighter II' - Special Champion Edition (USA).zip" sega-genesis\.
copy "Streets of Rage 2 (USA).zip" sega-genesis\.
copy "Streets of Rage 3 (USA).zip" sega-genesis\.
copy "Strider (USA, Europe).zip" sega-genesis\.
copy "Sub-Terrania (USA).zip" sega-genesis\.
copy "Sunset Riders (USA).zip" sega-genesis\.
copy "Super Smash T.V. (USA, Europe).zip" sega-genesis\.
copy "Super Street Fighter II (USA).zip" sega-genesis\.
copy "Super Thunder Blade (World).zip" sega-genesis\.
copy "Target Earth (USA).zip" sega-genesis\.
copy "Teenage Mutant Ninja Turtles - The Hyperstone Heist (USA).zip" sega-genesis\.
copy "Theme Park (USA, Europe).zip" sega-genesis\.
copy "Tiny Toon Adventures - Acme All-Stars (USA, Korea).zip" sega-genesis\.
copy "Tiny Toon Adventures - Buster's Hidden Treasure (USA).zip" sega-genesis\.
copy "ToeJam & Earl (World) (Rev A).zip" sega-genesis\.
copy "ToeJam & Earl in Panic on Funkotron (USA).zip" sega-genesis\.
copy "Top Gear 2 (USA).zip" sega-genesis\.
copy "Trouble Shooter (USA).zip" sega-genesis\.
copy "Two Crude Dudes (USA).zip" sega-genesis\.
copy "Ultimate Mortal Kombat 3 (USA).zip" sega-genesis\.
copy "Valis (USA).zip" sega-genesis\.
copy "Valis III (USA).zip" sega-genesis\.
copy "Vapor Trail (USA).zip" sega-genesis\.
copy "Vectorman (USA, Europe).zip" sega-genesis\.
copy "Vectorman 2 (USA).zip" sega-genesis\.
copy "Virtua Fighter 2 (USA, Europe).zip" sega-genesis\.
copy "Virtua Racing (USA).zip" sega-genesis\.
copy "World of Illusion Starring Mickey Mouse and Donald Duck (USA, Korea).zip" sega-genesis\.
copy "WWF Royal Rumble (World).zip" sega-genesis\.
copy "WWF Super WrestleMania (USA, Europe).zip" sega-genesis\.
copy "X-Men (USA).zip" sega-genesis\.
copy "X-Men 2 - Clone Wars (USA, Europe).zip" sega-genesis\.
copy "Ys III (USA).zip" sega-genesis\.
copy "Zany Golf (USA, Europe) (v1.1).zip" sega-genesis\.
copy "Zombies Ate My Neighbors (USA).zip" sega-genesis\.