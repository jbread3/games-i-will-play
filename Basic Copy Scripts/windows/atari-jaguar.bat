mkdir atari-jaguar
copy "Alien vs Predator (World).zip" atari-jaguar\.
copy "Attack of the Mutant Penguins (World).zip" atari-jaguar\.
copy "Checkered Flag (World).zip" atari-jaguar\.
copy "Doom (World).zip" atari-jaguar\.
copy "Double Dragon V - The Shadow Falls (World).zip" atari-jaguar\.
copy "NBA Jam - Tournament Edition (World).zip" atari-jaguar\.
copy "Pinball Fantasies (World).zip" atari-jaguar\.
copy "Raiden (World).zip" atari-jaguar\.
copy "Rayman (World).zip" atari-jaguar\.
copy "Worms (World).zip" atari-jaguar\.
copy "[BIOS] Atari Jaguar (World).zip" atari-jaguar\.
copy "[BIOS] Atari Jaguar CD (World).zip" atari-jaguar\.