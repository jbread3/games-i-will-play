mkdir snes
copy "ActRaiser (USA).zip" snes\.
copy "ActRaiser 2 (USA).zip" snes\.
copy "Aero Fighters (USA).zip" snes\.
copy "Aerobiz (USA).zip" snes\.
copy "Aladdin (USA).zip" snes\.
copy "B.O.B. (USA).zip" snes\.
copy "Battletoads in Battlemaniacs (USA).zip" snes\.
copy "Battletoads-Double Dragon (USA).zip" snes\.
copy "Biker Mice from Mars (USA).zip" snes\.
copy "Blackthorne (USA).zip" snes\.
copy "Bonkers (USA).zip" snes\.
copy "Boogerman - A Pick and Flick Adventure (USA).zip" snes\.
copy "Breath of Fire (USA).zip" snes\.
copy "Breath of Fire II (USA).zip" snes\.
copy "Bubsy in - Claws Encounters of the Furred Kind (USA).zip" snes\.
copy "Bugs Bunny - Rabbit Rampage (USA).zip" snes\.
copy "Bust-A-Move (USA).zip" snes\.
copy "Castlevania - Dracula X (USA).zip" snes\.
copy "Choplifter III - Rescue-Survive (USA).zip" snes\.
copy "Chrono Trigger (USA).zip" snes\.
copy "Clay Fighter (USA).zip" snes\.
copy "Claymates (USA).zip" snes\.
copy "Contra III - The Alien Wars (USA).zip" snes\.
copy "Cool Spot (USA).zip" snes\.
copy "Cool World (USA).zip" snes\.
copy "Daffy Duck - The Marvin Missions (USA).zip" snes\.
copy "Darius Twin (USA).zip" snes\.
copy "Demon's Crest (USA).zip" snes\.
copy "Donkey Kong Country (USA) (Rev 2).zip" snes\.
copy "Donkey Kong Country 2 - Diddy's Kong Quest (USA) (En,Fr) (Rev 1).zip" snes\.
copy "Donkey Kong Country 3 - Dixie Kong's Double Trouble! (USA) (En,Fr).zip" snes\.
copy "Doom (USA).zip" snes\.
copy "Doom Troopers - Mutant Chronicles (USA).zip" snes\.
copy "Dragon View (USA).zip" snes\.
copy "Dragon's Lair (USA).zip" snes\.
copy "Duel, The - Test Drive II (USA).zip" snes\.
copy "EarthBound (USA).zip" snes\.
copy "Earthworm Jim (USA).zip" snes\.
copy "Earthworm Jim 2 (USA).zip" snes\.
copy "F-Zero (USA).zip" snes\.
copy "Final Fantasy - Mystic Quest (USA) (Rev 1).zip" snes\.
copy "Final Fantasy II (USA) (Rev 1).zip" snes\.
copy "Final Fantasy III (USA) (Rev 1).zip" snes\.
copy "Final Fight (USA).zip" snes\.
copy "Final Fight 2 (USA).zip" snes\.
copy "Final Fight 3 (USA).zip" snes\.
copy "Final Fight Guy (USA).zip" snes\.
copy "Frogger (USA).zip" snes\.
copy "Gemfire (USA).zip" snes\.
copy "Goof Troop (USA).zip" snes\.
copy "Gradius III (USA).zip" snes\.
copy "GunForce (USA).zip" snes\.
copy "Harvest Moon (USA).zip" snes\.
copy "Illusion of Gaia (USA).zip" snes\.
copy "Jeopardy! - Deluxe Edition (USA).zip" snes\.
copy "Jimmy Connors Pro Tennis Tour (USA).zip" snes\.
copy "Joe & Mac (USA).zip" snes\.
copy "Joe & Mac 2 - Lost in the Tropics (USA).zip" snes\.
copy "Judge Dredd (USA).zip" snes\.
copy "Jungle Book, The (USA).zip" snes\.
copy "Jungle Strike (USA).zip" snes\.
copy "Jurassic Park (USA) (Rev 1).zip" snes\.
copy "Jurassic Park Part 2 - The Chaos Continues (USA) (En,Fr,De,It).zip" snes\.
copy "Ken Griffey Jr. Presents Major League Baseball (USA) (Rev 1).zip" snes\.
copy "Killer Instinct (USA) (Rev 1).zip" snes\.
copy "Kirby's Dream Course (USA).zip" snes\.
copy "Kirby's Dream Land 3 (USA).zip" snes\.
copy "Krusty's Super Fun House (USA) (Rev 1).zip" snes\.
copy "Legend of the Mystical Ninja, The (USA).zip" snes\.
copy "Legend of Zelda, The - A Link to the Past (USA).zip" snes\.
copy "Lemmings (USA) (Rev 1).zip" snes\.
copy "Lion King, The (USA).zip" snes\.
copy "Lost Vikings 2 (USA).zip" snes\.
copy "Lost Vikings, The (USA).zip" snes\.
copy "Lufia & the Fortress of Doom (USA).zip" snes\.
copy "Lufia II - Rise of the Sinistrals (USA).zip" snes\.
copy "Madden NFL '94 (USA).zip" snes\.
copy "Magical Quest Starring Mickey Mouse, The (USA).zip" snes\.
copy "Marvel Super Heroes in War of the Gems (USA).zip" snes\.
copy "Mega Man 7 (USA).zip" snes\.
copy "Mega Man X (USA) (Rev 1).zip" snes\.
copy "Mega Man X2 (USA).zip" snes\.
copy "Mega Man X3 (USA).zip" snes\.
copy "Mortal Kombat (USA).zip" snes\.
copy "Mortal Kombat 3 (USA).zip" snes\.
copy "Mortal Kombat II (USA) (Rev 1).zip" snes\.
copy "Mr. Do! (USA).zip" snes\.
copy "Mr. Nutz (USA) (En,Fr).zip" snes\.
copy "NBA Jam (USA) (Rev 1).zip" snes\.
copy "NBA Jam - Tournament Edition (USA).zip" snes\.
copy "New Horizons (USA).zip" snes\.
copy "NHL '94 (USA).zip" snes\.
copy "Ninja Gaiden Trilogy (USA).zip" snes\.
copy "Ogre Battle - The March of the Black Queen (USA).zip" snes\.
copy "Operation Thunderbolt (USA).zip" snes\.
copy "Out of this World (USA).zip" snes\.
copy "Paperboy 2 (USA).zip" snes\.
copy "Phalanx (USA).zip" snes\.
copy "Pilotwings (USA).zip" snes\.
copy "R-Type III (USA).zip" snes\.
copy "Raiden Trad (USA).zip" snes\.
copy "Rock n' Roll Racing (USA).zip" snes\.
copy "Secret of Evermore (USA).zip" snes\.
copy "Secret of Mana (USA).zip" snes\.
copy "Shaq-Fu (USA).zip" snes\.
copy "Side Pocket (USA).zip" snes\.
copy "SimCity (USA).zip" snes\.
copy "Simpsons, The - Bart's Nightmare (USA).zip" snes\.
copy "Skyblazer (USA).zip" snes\.
copy "Soul Blazer (USA).zip" snes\.
copy "Sparkster (USA).zip" snes\.
copy "Spider-Man - Venom - Maximum Carnage (USA).zip" snes\.
copy "Star Fox (USA) (Rev 2).zip" snes\.
copy "Street Fighter Alpha 2 (USA).zip" snes\.
copy "Street Fighter II (USA).zip" snes\.
copy "Street Fighter II Turbo (USA) (Rev 1).zip" snes\.
copy "Stunt Race FX (USA) (Rev 1).zip" snes\.
copy "Sunset Riders (USA).zip" snes\.
copy "Super Adventure Island (USA).zip" snes\.
copy "Super Adventure Island II (USA).zip" snes\.
copy "Super Bomberman (USA).zip" snes\.
copy "Super Bomberman 2 (USA).zip" snes\.
copy "Super Bonk (USA).zip" snes\.
copy "Super Castlevania IV (USA).zip" snes\.
copy "Super Chase H.Q. (USA).zip" snes\.
copy "Super Double Dragon (USA).zip" snes\.
copy "Super Ghouls'n Ghosts (USA).zip" snes\.
copy "Super Mario All-Stars + Super Mario World (USA).zip" snes\.
copy "Super Mario Kart (USA).zip" snes\.
copy "Super Mario RPG - Legend of the Seven Stars (USA).zip" snes\.
copy "Super Mario World (USA).zip" snes\.
copy "Super Mario World 2 - Yoshi's Island (USA).zip" snes\.
copy "Super Metroid (Japan, USA) (En,Ja).zip" snes\.
copy "Super Nova (USA).zip" snes\.
copy "Super Punch-Out!! (USA).zip" snes\.
copy "Super R-Type (USA).zip" snes\.
copy "Super Smash T.V. (USA).zip" snes\.
copy "Super Star Wars - Return of the Jedi (USA) (Rev 1).zip" snes\.
copy "Super Star Wars - The Empire Strikes Back (USA) (Rev 1).zip" snes\.
copy "Super Street Fighter II (USA).zip" snes\.
copy "Super Turrican (USA).zip" snes\.
copy "Super Turrican 2 (USA).zip" snes\.
copy "Tecmo Super Bowl (USA).zip" snes\.
copy "Tecmo Super Bowl II - Special Edition (USA).zip" snes\.
copy "Teenage Mutant Ninja Turtles - Tournament Fighters (USA).zip" snes\.
copy "Teenage Mutant Ninja Turtles IV - Turtles in Time (USA).zip" snes\.
copy "Tiny Toon Adventures - Buster Busts Loose! (USA).zip" snes\.
copy "Top Gear (USA).zip" snes\.
copy "Top Gear 2 (USA).zip" snes\.
copy "Top Gear 3000 (USA).zip" snes\.
copy "U.N. Squadron (USA).zip" snes\.
copy "Ultimate Mortal Kombat 3 (USA).zip" snes\.
copy "Uniracers (USA).zip" snes\.
copy "Vegas Stakes (USA).zip" snes\.
copy "Wheel of Fortune (USA).zip" snes\.
copy "Wild Guns (USA).zip" snes\.
copy "Wings 2 - Aces High (USA).zip" snes\.
copy "Wolfenstein 3-D (USA).zip" snes\.
copy "WWF Raw (USA).zip" snes\.
copy "WWF Royal Rumble (USA).zip" snes\.
copy "WWF Super WrestleMania (USA).zip" snes\.
copy "X-Men - Mutant Apocalypse (USA).zip" snes\.
copy "Ys III - Wanderers from Ys (USA).zip" snes\.
copy "Zombies Ate My Neighbors (USA).zip" snes\.