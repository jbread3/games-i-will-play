mkdir nintendo64
copy "007 - GoldenEye (USA).zip" nintendo64\.
copy "1080 TenEighty Snowboarding (Japan, USA) (En,Ja).zip" nintendo64\.
copy "Banjo-Kazooie (USA).zip" nintendo64\.
copy "Banjo-Tooie (USA).zip" nintendo64\.
copy "Bomberman 64 (USA).zip" nintendo64\.
copy "Carmageddon 64 (USA).zip" nintendo64\.
copy "Castlevania (USA).zip" nintendo64\.
copy "Conker's Bad Fur Day (USA).zip" nintendo64\.
copy "Cruis'n USA (USA).zip" nintendo64\.
copy "Cruis'n World (USA).zip" nintendo64\.
copy "Diddy Kong Racing (USA) (En,Fr).zip" nintendo64\.
copy "Donkey Kong 64 (USA).zip" nintendo64\.
copy "Doom 64 (USA).zip" nintendo64\.
copy "Excitebike 64 (USA).zip" nintendo64\.
copy "F-1 World Grand Prix (USA).zip" nintendo64\.
copy "F-Zero X (USA).zip" nintendo64\.
copy "Jet Force Gemini (USA).zip" nintendo64\.
copy "Kirby 64 - The Crystal Shards (USA).zip" nintendo64\.
copy "Legend of Zelda, The - Majora's Mask (USA).zip" nintendo64\.
copy "Legend of Zelda, The - Ocarina of Time (USA).zip" nintendo64\.
copy "Madden NFL 2002 (USA).zip" nintendo64\.
copy "Mario Golf (USA).zip" nintendo64\.
copy "Mario Kart 64 (USA).zip" nintendo64\.
copy "Mario Party (USA).zip" nintendo64\.
copy "Mario Party 2 (USA).zip" nintendo64\.
copy "Mario Party 3 (USA).zip" nintendo64\.
copy "NBA Hangtime (USA).zip" nintendo64\.
copy "Ogre Battle 64 - Person of Lordly Caliber (USA).zip" nintendo64\.
copy "Paper Mario (USA).zip" nintendo64\.
copy "Paperboy (USA).zip" nintendo64\.
copy "Perfect Dark (USA).zip" nintendo64\.
copy "Pilotwings 64 (USA).zip" nintendo64\.
copy "Star Fox 64 (USA).zip" nintendo64\.
copy "Super Mario 64 (USA).zip" nintendo64\.
copy "Super Smash Bros. (USA).zip" nintendo64\.
copy "Tony Hawk's Pro Skater (USA).zip" nintendo64\.
copy "Tony Hawk's Pro Skater 2 (USA).zip" nintendo64\.
copy "Tony Hawk's Pro Skater 3 (USA).zip" nintendo64\.
copy "Top Gear Overdrive (USA).zip" nintendo64\.
copy "Top Gear Rally 2 (USA).zip" nintendo64\.
copy "Turok - Dinosaur Hunter (USA).zip" nintendo64\.
copy "Turok 2 - Seeds of Evil (USA).zip" nintendo64\.
copy "Wave Race 64 (USA).zip" nintendo64\.
copy "WCW vs. nWo - World Tour (USA).zip" nintendo64\.
copy "Wipeout 64 (USA).zip" nintendo64\.
copy "WWF No Mercy (USA).zip" nintendo64\.
copy "Yoshi's Story (USA) (En,Ja).zip" nintendo64\.