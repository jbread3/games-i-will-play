mkdir playstation
copy "Castlevania - Symphony of the Night (USA) (Track 1).bin" playstation\.
copy "Castlevania - Symphony of the Night (USA) (Track 2).bin" playstation\.
copy "Castlevania - Symphony of the Night (USA).cue" playstation\.
copy "Castlevania - Symphony of the Night (USA).srm" playstation\.
copy "Einhander (USA).bin" playstation\.
copy "Einhander (USA).cue" playstation\.
copy "Einhander (USA).srm" playstation\.
copy "GTA2.pbp" playstation\.
copy "GTA2.srm" playstation\.
copy "Mega Man X4 (USA).bin" playstation\.
copy "Mega Man X4 (USA).cue" playstation\.
copy "Mega Man X4 (USA).srm" playstation\.
copy "Metal Gear Solid (G) (Disc 1) [SLES-01507].7z" playstation\.
copy "Metal Gear Solid (G) (Disc 1) [SLES-01507].bin.ecm" playstation\.
copy "Super Puzzle Fighter II Turbo (USA).srm" playstation\.
copy "THPS2.pbp" playstation\.
copy "THPS2.srm" playstation\.
copy "Tony Hawk's Pro Skater (USA).bin" playstation\.
copy "Tony Hawk's Pro Skater (USA).cue" playstation\.
copy "Tony Hawk's Pro Skater (USA).srm" playstation\.