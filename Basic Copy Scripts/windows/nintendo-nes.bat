mkdir nes
copy "1943 - The Battle of Midway (USA).zip" nes\.
copy "720 Degrees (USA).zip" nes\.
copy "Abadox - The Deadly Inner War (USA).zip" nes\.
copy "Addams Family, The (USA).zip" nes\.
copy "Adventure Island 3 (USA).zip" nes\.
copy "Adventure Island II (USA).zip" nes\.
copy "Adventures in the Magic Kingdom (USA).zip" nes\.
copy "Adventures of Lolo (USA).zip" nes\.
copy "Adventures of Lolo 2 (USA).zip" nes\.
copy "Adventures of Lolo 3 (USA).zip" nes\.
copy "American Gladiators (USA).zip" nes\.
copy "Balloon Fight (USA).zip" nes\.
copy "Baseball Stars (USA).zip" nes\.
copy "Baseball Stars II (USA).zip" nes\.
copy "Batman - The Video Game (USA).zip" nes\.
copy "Battle of Olympus, The (USA).zip" nes\.
copy "Battletoads (USA).zip" nes\.
copy "Battletoads-Double Dragon (USA).zip" nes\.
copy "Bionic Commando (USA).zip" nes\.
copy "Blades of Steel (USA).zip" nes\.
copy "Blaster Master (USA).zip" nes\.
copy "Bomberman (USA).zip" nes\.
copy "Bomberman II (USA).zip" nes\.
copy "Bubble Bobble (USA).zip" nes\.
copy "Bubble Bobble Part 2 (USA).zip" nes\.
copy "Bucky O'Hare (USA).zip" nes\.
copy "Bugs Bunny Birthday Blowout, The (USA).zip" nes\.
copy "Bugs Bunny Crazy Castle, The (USA).zip" nes\.
copy "Bump'n'Jump (USA).zip" nes\.
copy "BurgerTime (USA).zip" nes\.
copy "California Games (USA).zip" nes\.
copy "Casino Kid (USA).zip" nes\.
copy "Castlevania (USA) (Rev A).zip" nes\.
copy "Castlevania II - Simon's Quest (USA).zip" nes\.
copy "Castlevania III - Dracula's Curse (USA).zip" nes\.
copy "Chip 'n Dale - Rescue Rangers (USA).zip" nes\.
copy "Chip 'n Dale - Rescue Rangers 2 (USA).zip" nes\.
copy "Clash at Demonhead (USA).zip" nes\.
copy "Contra (USA).zip" nes\.
copy "Crystalis (USA).zip" nes\.
copy "Darkwing Duck (USA).zip" nes\.
copy "Defender II (USA).zip" nes\.
copy "Dick Tracy (USA).zip" nes\.
copy "Dig Dug II - Trouble in Paradise (USA).zip" nes\.
copy "Donkey Kong (World) (Rev A).zip" nes\.
copy "Donkey Kong 3 (World).zip" nes\.
copy "Double Dragon (USA).zip" nes\.
copy "Double Dragon II - The Revenge (USA).zip" nes\.
copy "Double Dragon III - The Sacred Stones (USA).zip" nes\.
copy "Dr. Mario (Japan, USA).zip" nes\.
copy "Dragon Warrior (USA) (Rev A).zip" nes\.
copy "Dragon Warrior II (USA).zip" nes\.
copy "Dragon Warrior III (USA).zip" nes\.
copy "Dragon Warrior IV (USA).zip" nes\.
copy "DuckTales (USA).zip" nes\.
copy "DuckTales 2 (USA).zip" nes\.
copy "Earth Bound (USA) (Proto).zip" nes\.
copy "Elevator Action (USA).zip" nes\.
copy "Faxanadu (USA).zip" nes\.
copy "Final Fantasy (USA).zip" nes\.
copy "Fire 'n Ice (USA).zip" nes\.
copy "G.I. Joe - A Real American Hero (USA).zip" nes\.
copy "G.I. Joe - The Atlantis Factor (USA).zip" nes\.
copy "Galaga - Demons of Death (USA).zip" nes\.
copy "Gargoyle's Quest II (USA).zip" nes\.
copy "Gauntlet (USA).zip" nes\.
copy "Ghosts'n Goblins (USA).zip" nes\.
copy "Goonies II, The (USA).zip" nes\.
copy "Gradius (USA).zip" nes\.
copy "Gun.Smoke (USA).zip" nes\.
copy "Hudson's Adventure Island (USA).zip" nes\.
copy "Ice Climber (USA, Europe).zip" nes\.
copy "Ice Hockey (USA).zip" nes\.
copy "IronSword - Wizards & Warriors II (USA).zip" nes\.
copy "Ivan 'Ironman' Stewart's Super Off Road (USA).zip" nes\.
copy "Jackal (USA).zip" nes\.
copy "Jaws (USA).zip" nes\.
copy "Jimmy Connors Tennis (USA).zip" nes\.
copy "Joust (USA).zip" nes\.
copy "Jurassic Park (USA).zip" nes\.
copy "Kickle Cubicle (USA).zip" nes\.
copy "Kid Icarus (USA, Europe).zip" nes\.
copy "Kirby's Adventure (USA) (Rev A).zip" nes\.
copy "Kiwi Kraze - A Bird-Brained Adventure! (USA).zip" nes\.
copy "Kung Fu (Japan, USA).zip" nes\.
copy "Life Force (USA).zip" nes\.
copy "Little Mermaid, The (USA).zip" nes\.
copy "Little Nemo - The Dream Master (USA).zip" nes\.
copy "Little Samson (USA).zip" nes\.
copy "Lode Runner (USA).zip" nes\.
copy "Maniac Mansion (USA).zip" nes\.
copy "Mappy-Land (USA).zip" nes\.
copy "Marble Madness (USA).zip" nes\.
copy "Mega Man (USA).zip" nes\.
copy "Mega Man 2 (USA).zip" nes\.
copy "Mega Man 3 (USA).zip" nes\.
copy "Mega Man 4 (USA) (Rev A).zip" nes\.
copy "Mega Man 5 (USA).zip" nes\.
copy "Mega Man 6 (USA).zip" nes\.
copy "Metal Gear (USA).zip" nes\.
copy "Metroid (USA).zip" nes\.
copy "Mickey Mousecapade (USA).zip" nes\.
copy "Micro Machines (USA) (Unl).zip" nes\.
copy "Mighty Final Fight (USA).zip" nes\.
copy "Milon's Secret Castle (USA).zip" nes\.
copy "Monster Party (USA).zip" nes\.
copy "Ms. Pac-Man (USA).zip" nes\.
copy "Ninja Gaiden (USA).zip" nes\.
copy "Ninja Gaiden II - The Dark Sword of Chaos (USA).zip" nes\.
copy "Ninja Gaiden III - The Ancient Ship of Doom (USA).zip" nes\.
copy "Nintendo World Cup (USA).zip" nes\.
copy "Pac-Man (USA) (Tengen).zip" nes\.
copy "Panic Restaurant (USA).zip" nes\.
copy "Paperboy (USA).zip" nes\.
copy "Paperboy 2 (USA).zip" nes\.
copy "Pinball Quest (USA).zip" nes\.
copy "Pipe Dream (USA).zip" nes\.
copy "Power Blade (USA).zip" nes\.
copy "Power Blade 2 (USA).zip" nes\.
copy "QIX (USA).zip" nes\.
copy "R.B.I. Baseball (USA).zip" nes\.
copy "R.B.I. Baseball 3 (USA) (Unl).zip" nes\.
copy "R.C. Pro-Am II (USA).zip" nes\.
copy "Rad Racer (USA).zip" nes\.
copy "Rad Racer II (USA).zip" nes\.
copy "Rampage (USA).zip" nes\.
copy "Rampart (USA).zip" nes\.
copy "River City Ransom (USA).zip" nes\.
copy "Rush'n Attack (USA).zip" nes\.
copy "Shatterhand (USA).zip" nes\.
copy "Simpsons, The - Bart vs. the Space Mutants (USA) (Rev A).zip" nes\.
copy "Skate or Die (USA).zip" nes\.
copy "Smash T.V. (USA).zip" nes\.
copy "Snake Rattle n Roll (USA).zip" nes\.
copy "Solomon's Key (USA).zip" nes\.
copy "Spelunker (USA).zip" nes\.
copy "Spy Hunter (USA).zip" nes\.
copy "StarTropics (USA).zip" nes\.
copy "Strider (USA).zip" nes\.
copy "Super C (USA).zip" nes\.
copy "Super Dodge Ball (USA).zip" nes\.
copy "Super Mario Bros. (World).zip" nes\.
copy "Super Mario Bros. 2 (USA) (Rev A).zip" nes\.
copy "Super Mario Bros. 3 (USA) (Rev A).zip" nes\.
copy "Super Spike V'Ball + Nintendo World Cup (USA).zip" nes\.
copy "Super Spy Hunter (USA).zip" nes\.
copy "TaleSpin (USA).zip" nes\.
copy "Tecmo Bowl (USA) (Rev A).zip" nes\.
copy "Tecmo Super Bowl (USA).zip" nes\.
copy "Teenage Mutant Ninja Turtles (USA).zip" nes\.
copy "Teenage Mutant Ninja Turtles II - The Arcade Game (USA).zip" nes\.
copy "Teenage Mutant Ninja Turtles III - The Manhattan Project (USA).zip" nes\.
copy "Tetris (USA).zip" nes\.
copy "Tiny Toon Adventures (USA).zip" nes\.
copy "Tiny Toon Adventures 2 - Trouble in Wackyland (USA).zip" nes\.
copy "Track & Field (USA).zip" nes\.
copy "Vegas Dream (USA).zip" nes\.
copy "Wall Street Kid (USA).zip" nes\.
copy "Whomp 'Em (USA).zip" nes\.
copy "Willow (USA).zip" nes\.
copy "Wizards & Warriors (USA) (Rev A).zip" nes\.
copy "Wizards & Warriors III - Kuros...Visions of Power (USA).zip" nes\.
copy "Zoda's Revenge - StarTropics II (USA).zip" nes\.