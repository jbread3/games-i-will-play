mkdir nec-turbografx16
copy "Aero Blasters (USA).zip" nec-turbografx16\.
copy "Air Zonk (USA).zip" nec-turbografx16\.
copy "Alien Crush (USA).zip" nec-turbografx16\.
copy "Ballistix (USA).zip" nec-turbografx16\.
copy "Blazing Lazers (USA).zip" nec-turbografx16\.
copy "Bomberman '93 (USA).zip" nec-turbografx16\.
copy "Bomberman (USA).zip" nec-turbografx16\.
copy "Bonk III - Bonk's Big Adventure (USA).zip" nec-turbografx16\.
copy "Bonk's Adventure (USA).zip" nec-turbografx16\.
copy "Bonk's Revenge (USA).zip" nec-turbografx16\.
copy "Bravoman (USA).zip" nec-turbografx16\.
copy "Darkwing Duck (USA).zip" nec-turbografx16\.
copy "Devil's Crush (USA).zip" nec-turbografx16\.
copy "Fantasy Zone (USA).zip" nec-turbografx16\.
copy "Final Lap Twin (USA).zip" nec-turbografx16\.
copy "Galaga '90 (USA).zip" nec-turbografx16\.
copy "J.J. & Jeff (USA).zip" nec-turbografx16\.
copy "Keith Courage in Alpha Zones (USA).zip" nec-turbografx16\.
copy "King of Casino (USA).zip" nec-turbografx16\.
copy "Legendary Axe II, The (USA).zip" nec-turbografx16\.
copy "Legendary Axe, The (USA).zip" nec-turbografx16\.
copy "Military Madness (USA).zip" nec-turbografx16\.
copy "Neutopia (USA).zip" nec-turbografx16\.
copy "New Adventure Island (USA).zip" nec-turbografx16\.
copy "Ninja Spirit (USA).zip" nec-turbografx16\.
copy "R-Type (USA).zip" nec-turbografx16\.
copy "Raiden (USA).zip" nec-turbografx16\.
copy "SideArms - Hyper Dyne (USA).zip" nec-turbografx16\.
copy "Splatterhouse (USA).zip" nec-turbografx16\.
copy "Street Fighter II' - Champion Edition (Japan).zip" nec-turbografx16\.
copy "Super Star Soldier (USA).zip" nec-turbografx16\.
copy "Super Volleyball (USA).zip" nec-turbografx16\.
copy "Yo, Bro (USA).zip" nec-turbografx16\.