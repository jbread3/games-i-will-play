mkdir sega-gamegear
copy "Ax Battler - A Legend of Golden Axe (USA, Europe).zip" sega-gamegear\.
copy "Castle of Illusion Starring Mickey Mouse (USA, Europe).zip" sega-gamegear\.
copy "Columns (USA, Europe).zip" sega-gamegear\.
copy "Deep Duck Trouble Starring Donald Duck (USA, Europe).zip" sega-gamegear\.
copy "Defenders of Oasis (USA, Europe).zip" sega-gamegear\.
copy "Dr. Robotnik's Mean Bean Machine (USA, Europe).zip" sega-gamegear\.
copy "Dragon Crystal (USA, Europe).zip" sega-gamegear\.
copy "G-LOC - Air Battle (USA, Europe).zip" sega-gamegear\.
copy "Gunstar Heroes (Japan).zip" sega-gamegear\.
copy "Mortal Kombat (USA, Europe).zip" sega-gamegear\.
copy "Mortal Kombat II (World).zip" sega-gamegear\.
copy "Shining Force II - The Sword of Hajya (USA).zip" sega-gamegear\.
copy "Shinobi (USA, Europe).zip" sega-gamegear\.
copy "Shinobi II - The Silent Fury (World) (Ja).zip" sega-gamegear\.
copy "Sonic Chaos (USA, Europe).zip" sega-gamegear\.
copy "Sonic Drift (Japan).zip" sega-gamegear\.
copy "Sonic Drift 2 (Japan, USA).zip" sega-gamegear\.
copy "Sonic The Hedgehog (World) (Rev 1).zip" sega-gamegear\.
copy "Sonic The Hedgehog - Triple Trouble (USA, Europe).zip" sega-gamegear\.
copy "Sonic The Hedgehog 2 (World).zip" sega-gamegear\.
copy "Sonic The Hedgehog Spinball (USA, Europe).zip" sega-gamegear\.
copy "Tails Adventures (World) (En,Ja).zip" sega-gamegear\.