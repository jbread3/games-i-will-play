mkdir gameboy
copy "Alleyway (World).zip" gameboy\.
copy "Castlevania - The Adventure (USA).zip" gameboy\.
copy "Castlevania II - Belmont's Revenge (USA, Europe).zip" gameboy\.
copy "Castlevania Legends (USA, Europe) (SGB Enhanced).zip" gameboy\.
copy "Contra - The Alien Wars (USA) (SGB Enhanced).zip" gameboy\.
copy "Donkey Kong (World) (Rev A) (SGB Enhanced).zip" gameboy\.
copy "Dr. Franken (USA).zip" gameboy\.
copy "Dr. Mario (World).zip" gameboy\.
copy "Final Fantasy Adventure (USA).zip" gameboy\.
copy "Final Fantasy Legend II (USA).zip" gameboy\.
copy "Final Fantasy Legend III (USA).zip" gameboy\.
copy "Final Fantasy Legend, The (USA).zip" gameboy\.
copy "Gargoyle's Quest (USA, Europe).zip" gameboy\.
copy "Kirby's Dream Land (USA, Europe).zip" gameboy\.
copy "Kirby's Dream Land 2 (USA, Europe) (SGB Enhanced).zip" gameboy\.
copy "Legend of Zelda, The - Link's Awakening (USA, Europe).zip" gameboy\.
copy "Legend of Zelda, The - Oracle of Ages (USA).zip" gameboy\.
copy "Legend of Zelda, The - Oracle of Seasons (USA).zip" gameboy\.
copy "Mario's Picross (USA, Europe) (SGB Enhanced).zip" gameboy\.
copy "Mega Man - Dr. Wily's Revenge (USA).zip" gameboy\.
copy "Mega Man II (USA).zip" gameboy\.
copy "Mega Man III (USA).zip" gameboy\.
copy "Mega Man IV (USA).zip" gameboy\.
copy "Metal Gear Solid (USA).zip" gameboy\.
copy "Metroid II - Return of Samus (World).zip" gameboy\.
copy "Mole Mania (USA, Europe) (SGB Enhanced).zip" gameboy\.
copy "Pocket Bomberman (Europe) (SGB Enhanced).zip" gameboy\.
copy "Pokemon Pinball (USA) (Rumble Version) (SGB Enhanced).zip" gameboy\.
copy "SolarStriker (World).zip" gameboy\.
copy "Super Mario Bros. Deluxe (USA, Europe).zip" gameboy\.
copy "Super Mario Land (World).zip" gameboy\.
copy "Super Mario Land 2 - 6 Golden Coins (USA, Europe).zip" gameboy\.
copy "Super Off Road (USA, Europe).zip" gameboy\.
copy "Super R.C. Pro-Am (USA, Europe).zip" gameboy\.
copy "Tetris (World) (Rev A).zip" gameboy\.
copy "Wario Land - Super Mario Land 3 (World).zip" gameboy\.
copy "Wario Land 3 (World) (En,Ja).zip" gameboy\.
copy "Wario Land II (USA, Europe) (SGB Enhanced).zip" gameboy\.