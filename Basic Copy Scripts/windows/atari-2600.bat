mkdir atari-2600
copy "Adventure.bin" atari-2600\.
copy "Berzerk.bin" atari-2600\.
copy "Centipede.bin" atari-2600\.
copy "Chopper Command (CCE).bin" atari-2600\.
copy "Demon Attack (Activision).bin" atari-2600\.
copy "Dig Dug.bin" atari-2600\.
copy "E.T..bin" atari-2600\.
copy "Joust.bin" atari-2600\.
copy "Moon Patrol.bin" atari-2600\.
copy "Pac-Man.bin" atari-2600\.
copy "Pitfall!.bin" atari-2600\.
copy "Pole Position.bin" atari-2600\.
copy "Q-bert.bin" atari-2600\.
copy "River Raid (16k Version).bin" atari-2600\.
copy "Space Invaders.bin" atari-2600\.
copy "Star Raiders.bin" atari-2600\.
copy "Super Breakout.bin" atari-2600\.
copy "Tapper (1984) (Sega - Bally Midway - Beck-Tech) (010-01).bin" atari-2600\.
