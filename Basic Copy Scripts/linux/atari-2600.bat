mkdir atari-2600
cp "Adventure.bin" atari-2600/.
cp "Berzerk.bin" atari-2600/.
cp "Centipede.bin" atari-2600/.
cp "Chopper Command (CCE).bin" atari-2600/.
cp "Demon Attack (Activision).bin" atari-2600/.
cp "Dig Dug.bin" atari-2600/.
cp "E.T..bin" atari-2600/.
cp "Joust.bin" atari-2600/.
cp "Moon Patrol.bin" atari-2600/.
cp "Pac-Man.bin" atari-2600/.
cp "Pitfall!.bin" atari-2600/.
cp "Pole Position.bin" atari-2600/.
cp "Q-bert.bin" atari-2600/.
cp "River Raid (16k Version).bin" atari-2600/.
cp "Space Invaders.bin" atari-2600/.
cp "Star Raiders.bin" atari-2600/.
cp "Super Breakout.bin" atari-2600/.
cp "Tapper (1984) (Sega - Bally Midway - Beck-Tech) (010-01).bin" atari-2600/.
