mkdir gameboy-advance
cp "Advance Wars (USA).zip" gameboy-advance/.
cp "Advance Wars 2 - Black Hole Rising (USA, Australia).zip" gameboy-advance/.
cp "Castlevania - Aria of Sorrow (USA).zip" gameboy-advance/.
cp "Castlevania - Circle of the Moon (USA).zip" gameboy-advance/.
cp "Final Fantasy I & II - Dawn of Souls (USA, Australia).zip" gameboy-advance/.
cp "Final Fantasy IV Advance (USA, Australia).zip" gameboy-advance/.
cp "Final Fantasy Tactics Advance (USA, Australia).zip" gameboy-advance/.
cp "Final Fantasy V Advance (USA).zip" gameboy-advance/.
cp "Final Fantasy VI Advance (USA).zip" gameboy-advance/.
cp "Final Fight One (USA).zip" gameboy-advance/.
cp "Fire Emblem (USA, Australia).zip" gameboy-advance/.
cp "Fire Emblem - The Sacred Stones (USA, Australia).zip" gameboy-advance/.
cp "Grand Theft Auto Advance (USA).zip" gameboy-advance/.
cp "Klonoa - Empire of Dreams (USA).zip" gameboy-advance/.
cp "Klonoa 2 - Dream Champ Tournament (USA).zip" gameboy-advance/.
cp "Legend of Spyro, The - A New Beginning (USA).zip" gameboy-advance/.
cp "Legend of Zelda, The - A Link to the Past & Four Swords (USA, Australia).zip" gameboy-advance/.
cp "Legend of Zelda, The - The Minish Cap (USA).zip" gameboy-advance/.
cp "Mario & Luigi - Superstar Saga (USA, Australia).zip" gameboy-advance/.
cp "Mario Golf - Advance Tour (USA).zip" gameboy-advance/.
cp "Mario Kart - Super Circuit (USA).zip" gameboy-advance/.
cp "Mario Pinball Land (USA, Australia).zip" gameboy-advance/.
cp "Mario Tennis - Power Tour (USA, Australia) (En,Fr,De,Es,It).zip" gameboy-advance/.
cp "Mario vs. Donkey Kong (USA, Australia).zip" gameboy-advance/.
cp "Mega Man & Bass (USA).zip" gameboy-advance/.
cp "Mega Man Zero 2 (USA).zip" gameboy-advance/.
cp "Mega Man Zero 3 (USA).zip" gameboy-advance/.
cp "Mega Man Zero 4 (USA).zip" gameboy-advance/.
cp "Metal Slug Advance (USA).zip" gameboy-advance/.
cp "Metroid - Zero Mission (USA).zip" gameboy-advance/.
cp "Metroid Fusion (USA, Australia).zip" gameboy-advance/.
cp "Teenage Mutant Ninja Turtles (USA).zip" gameboy-advance/.
cp "Teenage Mutant Ninja Turtles 2 - Battle Nexus (USA).zip" gameboy-advance/.
cp "Tony Hawk's Pro Skater 2 (USA, Europe).zip" gameboy-advance/.
cp "Tony Hawk's Pro Skater 3 (USA, Europe).zip" gameboy-advance/.
cp "Tony Hawk's Pro Skater 4 (USA, Europe).zip" gameboy-advance/.
cp "Wario Land 4 (USA, Europe).zip" gameboy-advance/.