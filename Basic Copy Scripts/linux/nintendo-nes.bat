mkdir nes
cp "1943 - The Battle of Midway (USA).zip" nes/.
cp "720 Degrees (USA).zip" nes/.
cp "Abadox - The Deadly Inner War (USA).zip" nes/.
cp "Addams Family, The (USA).zip" nes/.
cp "Adventure Island 3 (USA).zip" nes/.
cp "Adventure Island II (USA).zip" nes/.
cp "Adventures in the Magic Kingdom (USA).zip" nes/.
cp "Adventures of Lolo (USA).zip" nes/.
cp "Adventures of Lolo 2 (USA).zip" nes/.
cp "Adventures of Lolo 3 (USA).zip" nes/.
cp "American Gladiators (USA).zip" nes/.
cp "Balloon Fight (USA).zip" nes/.
cp "Baseball Stars (USA).zip" nes/.
cp "Baseball Stars II (USA).zip" nes/.
cp "Batman - The Video Game (USA).zip" nes/.
cp "Battle of Olympus, The (USA).zip" nes/.
cp "Battletoads (USA).zip" nes/.
cp "Battletoads-Double Dragon (USA).zip" nes/.
cp "Bionic Commando (USA).zip" nes/.
cp "Blades of Steel (USA).zip" nes/.
cp "Blaster Master (USA).zip" nes/.
cp "Bomberman (USA).zip" nes/.
cp "Bomberman II (USA).zip" nes/.
cp "Bubble Bobble (USA).zip" nes/.
cp "Bubble Bobble Part 2 (USA).zip" nes/.
cp "Bucky O'Hare (USA).zip" nes/.
cp "Bugs Bunny Birthday Blowout, The (USA).zip" nes/.
cp "Bugs Bunny Crazy Castle, The (USA).zip" nes/.
cp "Bump'n'Jump (USA).zip" nes/.
cp "BurgerTime (USA).zip" nes/.
cp "California Games (USA).zip" nes/.
cp "Casino Kid (USA).zip" nes/.
cp "Castlevania (USA) (Rev A).zip" nes/.
cp "Castlevania II - Simon's Quest (USA).zip" nes/.
cp "Castlevania III - Dracula's Curse (USA).zip" nes/.
cp "Chip 'n Dale - Rescue Rangers (USA).zip" nes/.
cp "Chip 'n Dale - Rescue Rangers 2 (USA).zip" nes/.
cp "Clash at Demonhead (USA).zip" nes/.
cp "Contra (USA).zip" nes/.
cp "Crystalis (USA).zip" nes/.
cp "Darkwing Duck (USA).zip" nes/.
cp "Defender II (USA).zip" nes/.
cp "Dick Tracy (USA).zip" nes/.
cp "Dig Dug II - Trouble in Paradise (USA).zip" nes/.
cp "Donkey Kong (World) (Rev A).zip" nes/.
cp "Donkey Kong 3 (World).zip" nes/.
cp "Double Dragon (USA).zip" nes/.
cp "Double Dragon II - The Revenge (USA).zip" nes/.
cp "Double Dragon III - The Sacred Stones (USA).zip" nes/.
cp "Dr. Mario (Japan, USA).zip" nes/.
cp "Dragon Warrior (USA) (Rev A).zip" nes/.
cp "Dragon Warrior II (USA).zip" nes/.
cp "Dragon Warrior III (USA).zip" nes/.
cp "Dragon Warrior IV (USA).zip" nes/.
cp "DuckTales (USA).zip" nes/.
cp "DuckTales 2 (USA).zip" nes/.
cp "Earth Bound (USA) (Proto).zip" nes/.
cp "Elevator Action (USA).zip" nes/.
cp "Faxanadu (USA).zip" nes/.
cp "Final Fantasy (USA).zip" nes/.
cp "Fire 'n Ice (USA).zip" nes/.
cp "G.I. Joe - A Real American Hero (USA).zip" nes/.
cp "G.I. Joe - The Atlantis Factor (USA).zip" nes/.
cp "Galaga - Demons of Death (USA).zip" nes/.
cp "Gargoyle's Quest II (USA).zip" nes/.
cp "Gauntlet (USA).zip" nes/.
cp "Ghosts'n Goblins (USA).zip" nes/.
cp "Goonies II, The (USA).zip" nes/.
cp "Gradius (USA).zip" nes/.
cp "Gun.Smoke (USA).zip" nes/.
cp "Hudson's Adventure Island (USA).zip" nes/.
cp "Ice Climber (USA, Europe).zip" nes/.
cp "Ice Hockey (USA).zip" nes/.
cp "IronSword - Wizards & Warriors II (USA).zip" nes/.
cp "Ivan 'Ironman' Stewart's Super Off Road (USA).zip" nes/.
cp "Jackal (USA).zip" nes/.
cp "Jaws (USA).zip" nes/.
cp "Jimmy Connors Tennis (USA).zip" nes/.
cp "Joust (USA).zip" nes/.
cp "Jurassic Park (USA).zip" nes/.
cp "Kickle Cubicle (USA).zip" nes/.
cp "Kid Icarus (USA, Europe).zip" nes/.
cp "Kirby's Adventure (USA) (Rev A).zip" nes/.
cp "Kiwi Kraze - A Bird-Brained Adventure! (USA).zip" nes/.
cp "Kung Fu (Japan, USA).zip" nes/.
cp "Life Force (USA).zip" nes/.
cp "Little Mermaid, The (USA).zip" nes/.
cp "Little Nemo - The Dream Master (USA).zip" nes/.
cp "Little Samson (USA).zip" nes/.
cp "Lode Runner (USA).zip" nes/.
cp "Maniac Mansion (USA).zip" nes/.
cp "Mappy-Land (USA).zip" nes/.
cp "Marble Madness (USA).zip" nes/.
cp "Mega Man (USA).zip" nes/.
cp "Mega Man 2 (USA).zip" nes/.
cp "Mega Man 3 (USA).zip" nes/.
cp "Mega Man 4 (USA) (Rev A).zip" nes/.
cp "Mega Man 5 (USA).zip" nes/.
cp "Mega Man 6 (USA).zip" nes/.
cp "Metal Gear (USA).zip" nes/.
cp "Metroid (USA).zip" nes/.
cp "Mickey Mousecapade (USA).zip" nes/.
cp "Micro Machines (USA) (Unl).zip" nes/.
cp "Mighty Final Fight (USA).zip" nes/.
cp "Milon's Secret Castle (USA).zip" nes/.
cp "Monster Party (USA).zip" nes/.
cp "Ms. Pac-Man (USA).zip" nes/.
cp "Ninja Gaiden (USA).zip" nes/.
cp "Ninja Gaiden II - The Dark Sword of Chaos (USA).zip" nes/.
cp "Ninja Gaiden III - The Ancient Ship of Doom (USA).zip" nes/.
cp "Nintendo World Cup (USA).zip" nes/.
cp "Pac-Man (USA) (Tengen).zip" nes/.
cp "Panic Restaurant (USA).zip" nes/.
cp "Paperboy (USA).zip" nes/.
cp "Paperboy 2 (USA).zip" nes/.
cp "Pinball Quest (USA).zip" nes/.
cp "Pipe Dream (USA).zip" nes/.
cp "Power Blade (USA).zip" nes/.
cp "Power Blade 2 (USA).zip" nes/.
cp "QIX (USA).zip" nes/.
cp "R.B.I. Baseball (USA).zip" nes/.
cp "R.B.I. Baseball 3 (USA) (Unl).zip" nes/.
cp "R.C. Pro-Am II (USA).zip" nes/.
cp "Rad Racer (USA).zip" nes/.
cp "Rad Racer II (USA).zip" nes/.
cp "Rampage (USA).zip" nes/.
cp "Rampart (USA).zip" nes/.
cp "River City Ransom (USA).zip" nes/.
cp "Rush'n Attack (USA).zip" nes/.
cp "Shatterhand (USA).zip" nes/.
cp "Simpsons, The - Bart vs. the Space Mutants (USA) (Rev A).zip" nes/.
cp "Skate or Die (USA).zip" nes/.
cp "Smash T.V. (USA).zip" nes/.
cp "Snake Rattle n Roll (USA).zip" nes/.
cp "Solomon's Key (USA).zip" nes/.
cp "Spelunker (USA).zip" nes/.
cp "Spy Hunter (USA).zip" nes/.
cp "StarTropics (USA).zip" nes/.
cp "Strider (USA).zip" nes/.
cp "Super C (USA).zip" nes/.
cp "Super Dodge Ball (USA).zip" nes/.
cp "Super Mario Bros. (World).zip" nes/.
cp "Super Mario Bros. 2 (USA) (Rev A).zip" nes/.
cp "Super Mario Bros. 3 (USA) (Rev A).zip" nes/.
cp "Super Spike V'Ball + Nintendo World Cup (USA).zip" nes/.
cp "Super Spy Hunter (USA).zip" nes/.
cp "TaleSpin (USA).zip" nes/.
cp "Tecmo Bowl (USA) (Rev A).zip" nes/.
cp "Tecmo Super Bowl (USA).zip" nes/.
cp "Teenage Mutant Ninja Turtles (USA).zip" nes/.
cp "Teenage Mutant Ninja Turtles II - The Arcade Game (USA).zip" nes/.
cp "Teenage Mutant Ninja Turtles III - The Manhattan Project (USA).zip" nes/.
cp "Tetris (USA).zip" nes/.
cp "Tiny Toon Adventures (USA).zip" nes/.
cp "Tiny Toon Adventures 2 - Trouble in Wackyland (USA).zip" nes/.
cp "Track & Field (USA).zip" nes/.
cp "Vegas Dream (USA).zip" nes/.
cp "Wall Street Kid (USA).zip" nes/.
cp "Whomp 'Em (USA).zip" nes/.
cp "Willow (USA).zip" nes/.
cp "Wizards & Warriors (USA) (Rev A).zip" nes/.
cp "Wizards & Warriors III - Kuros...Visions of Power (USA).zip" nes/.
cp "Zoda's Revenge - StarTropics II (USA).zip" nes/.