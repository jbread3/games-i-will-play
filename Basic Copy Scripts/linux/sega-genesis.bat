mkdir sega-genesis
cp "Addams Family, The (USA, Europe).zip" sega-genesis/.
cp "Adventures of Batman & Robin, The (USA).zip" sega-genesis/.
cp "Aerobiz (USA).zip" sega-genesis/.
cp "After Burner II (USA, Europe).zip" sega-genesis/.
cp "Aladdin (USA).zip" sega-genesis/.
cp "Alex Kidd in the Enchanted Castle (USA).zip" sega-genesis/.
cp "Altered Beast (USA, Europe).zip" sega-genesis/.
cp "Atomic Runner (USA).zip" sega-genesis/.
cp "Batman - The Video Game (USA).zip" sega-genesis/.
cp "Batman Returns (World).zip" sega-genesis/.
cp "BattleTech - A Game of Armored Combat (USA).zip" sega-genesis/.
cp "Battletoads (World).zip" sega-genesis/.
cp "Battletoads-Double Dragon (USA).zip" sega-genesis/.
cp "Beast Wrestler (USA).zip" sega-genesis/.
cp "Beyond Oasis (USA).zip" sega-genesis/.
cp "Bio Hazard Battle (USA, Europe).zip
cp "Blades of Vengeance (USA, Europe).zip" sega-genesis/.
cp "Blaster Master 2 (USA).zip" sega-genesis/.
cp "Bonanza Bros. (World) (Rev B).zip" sega-genesis/.
cp "Boogerman - A Pick and Flick Adventure (USA).zip" sega-genesis/.
cp "Bubble and Squeak (USA).zip" sega-genesis/.
cp "Bubsy in - Claws Encounters of the Furred Kind (USA, Europe).zip" sega-genesis/.
cp "Buck Rogers - Countdown to Doomsday (USA, Europe).zip" sega-genesis/.
cp "Burning Force (USA).zip" sega-genesis/.
cp "Cadash (USA, Korea).zip" sega-genesis/.
cp "California Games (USA, Europe).zip" sega-genesis/.
cp "Castle of Illusion Starring Mickey Mouse (USA, Europe).zip" sega-genesis/.
cp "Castlevania - Bloodlines (USA).zip" sega-genesis/.
cp "Championship Pro-Am (USA).zip" sega-genesis/.
cp "Columns III (USA).zip" sega-genesis/.
cp "Combat Cars (USA, Europe).zip" sega-genesis/.
cp "Comix Zone (USA).zip" sega-genesis/.
cp "Contra - Hard Corps (USA, Korea).zip" sega-genesis/.
cp "Cool Spot (USA).zip" sega-genesis/.
cp "Crack Down (USA).zip" sega-genesis/.
cp "Cyborg Justice (USA, Europe).zip" sega-genesis/.
cp "DEcapAttack (USA, Europe).zip" sega-genesis/.
cp "Demolition Man (USA, Europe).zip" sega-genesis/.
cp "Desert Demolition Starring Road Runner and Wile E. Coyote (USA, Europe).zip" sega-genesis/.
cp "Desert Strike - Return to the Gulf (USA, Europe).zip" sega-genesis/.
cp "Dick Tracy (World).zip" sega-genesis/.
cp "Doom Troopers (USA).zip" sega-genesis/.
cp "Double Dragon 3 - The Arcade Game (USA, Europe).zip" sega-genesis/.
cp "Dr. Robotnik's Mean Bean Machine (USA).zip" sega-genesis/.
cp "Dragon's Fury (USA, Europe).zip" sega-genesis/.
cp "Duke Nukem 3D (Brazil).zip" sega-genesis/.
cp "Dynamite Headdy (USA, Europe).zip" sega-genesis/.
cp "Earthworm Jim (USA).zip" sega-genesis/.
cp "Earthworm Jim 2 (USA).zip" sega-genesis/.
cp "Ecco - The Tides of Time (USA).zip" sega-genesis/.
cp "Ecco the Dolphin (USA, Europe).zip" sega-genesis/.
cp "Eternal Champions (USA).zip" sega-genesis/.
cp "Ex-Mutants (USA, Europe).zip" sega-genesis/.
cp "Faery Tale Adventure, The (USA, Europe).zip" sega-genesis/.
cp "Fantastic Dizzy (USA, Europe) (En,Fr,De,Es,It).zip" sega-genesis/.
cp "Fatal Fury 2 (USA, Korea).zip" sega-genesis/.
cp "FIFA Soccer 95 (USA, Europe) (En,Fr,De,Es).zip" sega-genesis/.
cp "Fighting Masters (USA).zip" sega-genesis/.
cp "Fire Shark (USA).zip" sega-genesis/.
cp "Flicky (USA, Europe).zip" sega-genesis/.
cp "Frogger (USA).zip" sega-genesis/.
cp "Gain Ground (World) (Rev A).zip" sega-genesis/.
cp "Galaxy Force II (World) (Rev B).zip" sega-genesis/.
cp "Gargoyles (USA).zip" sega-genesis/.
cp "Gauntlet IV (USA, Europe) (En,Ja) (September 1993).zip" sega-genesis/.
cp "Gemfire (USA).zip" sega-genesis/.
cp "General Chaos (USA, Europe).zip" sega-genesis/.
cp "Ghostbusters (World) (v1.1).zip" sega-genesis/.
cp "Ghouls'n Ghosts (USA, Europe) (Rev A).zip" sega-genesis/.
cp "Golden Axe (World) (v1.1).zip" sega-genesis/.
cp "Golden Axe II (World).zip" sega-genesis/.
cp "Great Circus Mystery Starring Mickey & Minnie, The (USA).zip" sega-genesis/.
cp "Grind Stormer (USA).zip" sega-genesis/.
cp "Gunstar Heroes (USA).zip" sega-genesis/.
cp "Haunting Starring Polterguy (USA, Europe).zip" sega-genesis/.
cp "Hellfire (USA).zip" sega-genesis/.
cp "Herzog Zwei (USA, Europe).zip" sega-genesis/.
cp "High Seas Havoc (USA).zip" sega-genesis/.
cp "Immortal, The (USA, Europe).zip" sega-genesis/.
cp "James Pond - Underwater Agent (USA, Europe).zip" sega-genesis/.
cp "James Pond 3 (USA, Europe).zip" sega-genesis/.
cp "James Pond II - Codename - Robocod (USA, Europe).zip" sega-genesis/.
cp "Jungle Book, The (USA).zip" sega-genesis/.
cp "Jungle Strike (USA, Europe).zip" sega-genesis/.
cp "Jurassic Park (USA).zip" sega-genesis/.
cp "Jurassic Park - Rampage Edition (USA, Europe).zip" sega-genesis/.
cp "Kid Chameleon (USA, Europe).zip" sega-genesis/.
cp "King of the Monsters (USA).zip" sega-genesis/.
cp "King of the Monsters 2 (USA).zip" sega-genesis/.
cp "King's Bounty - The Conqueror's Quest (USA, Europe).zip" sega-genesis/.
cp "Klax (USA, Europe).zip" sega-genesis/.
cp "Landstalker (USA).zip" sega-genesis/.
cp "Last Battle (USA, Europe).zip" sega-genesis/.
cp "Light Crusader (USA).zip" sega-genesis/.
cp "Lion King, The (World).zip" sega-genesis/.
cp "Lost Vikings, The (USA).zip" sega-genesis/.
cp "Lotus Turbo Challenge (USA, Europe).zip" sega-genesis/.
cp "Madden NFL 95 (USA, Europe).zip" sega-genesis/.
cp "Marvel Land (USA).zip" sega-genesis/.
cp "Master of Monsters (USA).zip" sega-genesis/.
cp "Mazin Saga - Mutant Fighter (USA).zip" sega-genesis/.
cp "Mega Bomberman (USA).zip" sega-genesis/.
cp "Mega Turrican (USA).zip" sega-genesis/.
cp "Mickey Mania - The Timeless Adventures of Mickey Mouse (USA).zip" sega-genesis/.
cp "Micro Machines (USA, Europe) (Alt 1).zip" sega-genesis/.
cp "Mighty Morphin Power Rangers (USA).zip" sega-genesis/.
cp "Mighty Morphin Power Rangers - The Movie (USA).zip" sega-genesis/.
cp "Mortal Kombat (World).zip" sega-genesis/.
cp "Mortal Kombat 3 (USA).zip" sega-genesis/.
cp "Mortal Kombat II (World).zip" sega-genesis/.
cp "Ms. Pac-Man (USA, Europe).zip" sega-genesis/.
cp "Mutant League Football (USA, Europe).zip" sega-genesis/.
cp "Mutant League Hockey (USA, Europe).zip" sega-genesis/.
cp "NBA Jam - Tournament Edition (World).zip" sega-genesis/.
cp "NBA Live 95 (USA, Europe).zip" sega-genesis/.
cp "NHL '94 (USA, Europe).zip" sega-genesis/.
cp "Outlander (USA).zip" sega-genesis/.
cp "OutRun (USA, Europe).zip" sega-genesis/.
cp "OutRun 2019 (USA).zip" sega-genesis/.
cp "Pac-Attack (USA).zip" sega-genesis/.
cp "Paperboy (USA, Europe).zip" sega-genesis/.
cp "Phantasy Star II (USA, Europe) (Rev A).zip" sega-genesis/.
cp "Pirates! Gold (USA).zip" sega-genesis/.
cp "Populous (USA).zip" sega-genesis/.
cp "Prince of Persia (USA).zip" sega-genesis/.
cp "Puggsy (USA).zip" sega-genesis/.
cp "Punisher, The (USA).zip" sega-genesis/.
cp "QuackShot Starring Donald Duck ~ QuackShot - I Love Donald Duck - Guruzia Ou no Hihou (World) (v1.1).zip" sega-genesis/.
cp "Rambo III (World) (v1.1).zip" sega-genesis/.
cp "Ren & Stimpy Show Presents, The - Stimpy's Invention (USA).zip" sega-genesis/.
cp "Revenge of Shinobi, The (USA, Europe).zip" sega-genesis/.
cp "Rings of Power (USA, Europe).zip" sega-genesis/.
cp "Ristar (USA, Europe) (September 1994).zip" sega-genesis/.
cp "Road Rash (USA, Europe).zip" sega-genesis/.
cp "Road Rash 3 (USA, Europe).zip" sega-genesis/.
cp "Road Rash II (USA, Europe) (v1.2).zip" sega-genesis/.
cp "RoadBlasters (USA).zip" sega-genesis/.
cp "RoboCop versus The Terminator (USA).zip" sega-genesis/.
cp "Rock n' Roll Racing (USA).zip" sega-genesis/.
cp "Rocket Knight Adventures (USA).zip" sega-genesis/.
cp "Samurai Shodown (USA).zip" sega-genesis/.
cp "Shadow Dancer - The Secret of Shinobi (World).zip" sega-genesis/.
cp "Shadowrun (USA).zip" sega-genesis/.
cp "Shaq-Fu (USA, Europe).zip" sega-genesis/.
cp "Shining Force (USA).zip" sega-genesis/.
cp "Shining Force II (USA).zip" sega-genesis/.
cp "Shinobi III - Return of the Ninja Master (USA).zip" sega-genesis/.
cp "Side Pocket (USA).zip" sega-genesis/.
cp "Skeleton Krew (USA).zip" sega-genesis/.
cp "Sonic & Knuckles (World).zip" sega-genesis/.
cp "Sonic 3D Blast ~ Sonic 3D Flickies' Island (USA, Europe).zip" sega-genesis/.
cp "Sonic The Hedgehog (USA, Europe).zip" sega-genesis/.
cp "Sonic The Hedgehog 2 (World) (Rev A).zip" sega-genesis/.
cp "Sonic The Hedgehog 3 (USA).zip" sega-genesis/.
cp "Sonic The Hedgehog Spinball (USA) (Alt 1).zip" sega-genesis/.
cp "Sparkster (USA).zip" sega-genesis/.
cp "Speedball 2 - Brutal Deluxe (USA).zip" sega-genesis/.
cp "Splatterhouse 2 (USA).zip" sega-genesis/.
cp "Splatterhouse 3 (USA).zip" sega-genesis/.
cp "Star Control (USA) (Unl).zip" sega-genesis/.
cp "Starflight (USA, Europe) (v1.1).zip" sega-genesis/.
cp "Steel Empire (USA).zip" sega-genesis/.
cp "Street Fighter II' - Special Champion Edition (USA).zip" sega-genesis/.
cp "Streets of Rage 2 (USA).zip" sega-genesis/.
cp "Streets of Rage 3 (USA).zip" sega-genesis/.
cp "Strider (USA, Europe).zip" sega-genesis/.
cp "Sub-Terrania (USA).zip" sega-genesis/.
cp "Sunset Riders (USA).zip" sega-genesis/.
cp "Super Smash T.V. (USA, Europe).zip" sega-genesis/.
cp "Super Street Fighter II (USA).zip" sega-genesis/.
cp "Super Thunder Blade (World).zip" sega-genesis/.
cp "Target Earth (USA).zip" sega-genesis/.
cp "Teenage Mutant Ninja Turtles - The Hyperstone Heist (USA).zip" sega-genesis/.
cp "Theme Park (USA, Europe).zip" sega-genesis/.
cp "Tiny Toon Adventures - Acme All-Stars (USA, Korea).zip" sega-genesis/.
cp "Tiny Toon Adventures - Buster's Hidden Treasure (USA).zip" sega-genesis/.
cp "ToeJam & Earl (World) (Rev A).zip" sega-genesis/.
cp "ToeJam & Earl in Panic on Funkotron (USA).zip" sega-genesis/.
cp "Top Gear 2 (USA).zip" sega-genesis/.
cp "Trouble Shooter (USA).zip" sega-genesis/.
cp "Two Crude Dudes (USA).zip" sega-genesis/.
cp "Ultimate Mortal Kombat 3 (USA).zip" sega-genesis/.
cp "Valis (USA).zip" sega-genesis/.
cp "Valis III (USA).zip" sega-genesis/.
cp "Vapor Trail (USA).zip" sega-genesis/.
cp "Vectorman (USA, Europe).zip" sega-genesis/.
cp "Vectorman 2 (USA).zip" sega-genesis/.
cp "Virtua Fighter 2 (USA, Europe).zip" sega-genesis/.
cp "Virtua Racing (USA).zip" sega-genesis/.
cp "World of Illusion Starring Mickey Mouse and Donald Duck (USA, Korea).zip" sega-genesis/.
cp "WWF Royal Rumble (World).zip" sega-genesis/.
cp "WWF Super WrestleMania (USA, Europe).zip" sega-genesis/.
cp "X-Men (USA).zip" sega-genesis/.
cp "X-Men 2 - Clone Wars (USA, Europe).zip" sega-genesis/.
cp "Ys III (USA).zip" sega-genesis/.
cp "Zany Golf (USA, Europe) (v1.1).zip" sega-genesis/.
cp "Zombies Ate My Neighbors (USA).zip" sega-genesis/.