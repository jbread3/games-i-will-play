mkdir playstation
cp "Castlevania - Symphony of the Night (USA) (Track 1).bin" playstation/.
cp "Castlevania - Symphony of the Night (USA) (Track 2).bin" playstation/.
cp "Castlevania - Symphony of the Night (USA).cue" playstation/.
cp "Castlevania - Symphony of the Night (USA).srm" playstation/.
cp "Einhander (USA).bin" playstation/.
cp "Einhander (USA).cue" playstation/.
cp "Einhander (USA).srm" playstation/.
cp "GTA2.pbp" playstation/.
cp "GTA2.srm" playstation/.
cp "Mega Man X4 (USA).bin" playstation/.
cp "Mega Man X4 (USA).cue" playstation/.
cp "Mega Man X4 (USA).srm" playstation/.
cp "Metal Gear Solid (G) (Disc 1) [SLES-01507].7z" playstation/.
cp "Metal Gear Solid (G) (Disc 1) [SLES-01507].bin.ecm" playstation/.
cp "Super Puzzle Fighter II Turbo (USA).srm" playstation/.
cp "THPS2.pbp" playstation/.
cp "THPS2.srm" playstation/.
cp "Tony Hawk's Pro Skater (USA).bin" playstation/.
cp "Tony Hawk's Pro Skater (USA).cue" playstation/.
cp "Tony Hawk's Pro Skater (USA).srm" playstation/.