mkdir atari-lynx
cp "Blue Lightning (USA, Europe).zip" atari-lynx/.
cp "California Games (USA, Europe).zip" atari-lynx/.
cp "Checkered Flag (USA, Europe).zip" atari-lynx/.
cp "Chip's Challenge (USA, Europe).zip" atari-lynx/.
cp "Electrocop (USA, Europe).zip" atari-lynx/.
cp "Klax (USA, Europe).zip" atari-lynx/.
cp "Raiden (USA) (v3.0).zip" atari-lynx/.
cp "Rampage (USA, Europe).zip" atari-lynx/.
cp "Rampart (USA, Europe).zip" atari-lynx/.
cp "Robotron 2084 (USA, Europe).zip" atari-lynx/.
cp "Rygar - Legendary Warrior (USA, Europe).zip" atari-lynx/.
cp "S.T.U.N. Runner (USA, Europe).zip" atari-lynx/.
cp "Todd's Adventures in Slime World (USA, Europe).zip" atari-lynx/.
cp "Warbirds (USA, Europe).zip" atari-lynx/.