mkdir sega-gamegear
cp "Ax Battler - A Legend of Golden Axe (USA, Europe).zip" sega-gamegear/.
cp "Castle of Illusion Starring Mickey Mouse (USA, Europe).zip" sega-gamegear/.
cp "Columns (USA, Europe).zip" sega-gamegear/.
cp "Deep Duck Trouble Starring Donald Duck (USA, Europe).zip" sega-gamegear/.
cp "Defenders of Oasis (USA, Europe).zip" sega-gamegear/.
cp "Dr. Robotnik's Mean Bean Machine (USA, Europe).zip" sega-gamegear/.
cp "Dragon Crystal (USA, Europe).zip" sega-gamegear/.
cp "G-LOC - Air Battle (USA, Europe).zip" sega-gamegear/.
cp "Gunstar Heroes (Japan).zip" sega-gamegear/.
cp "Mortal Kombat (USA, Europe).zip" sega-gamegear/.
cp "Mortal Kombat II (World).zip" sega-gamegear/.
cp "Shining Force II - The Sword of Hajya (USA).zip" sega-gamegear/.
cp "Shinobi (USA, Europe).zip" sega-gamegear/.
cp "Shinobi II - The Silent Fury (World) (Ja).zip" sega-gamegear/.
cp "Sonic Chaos (USA, Europe).zip" sega-gamegear/.
cp "Sonic Drift (Japan).zip" sega-gamegear/.
cp "Sonic Drift 2 (Japan, USA).zip" sega-gamegear/.
cp "Sonic The Hedgehog (World) (Rev 1).zip" sega-gamegear/.
cp "Sonic The Hedgehog - Triple Trouble (USA, Europe).zip" sega-gamegear/.
cp "Sonic The Hedgehog 2 (World).zip" sega-gamegear/.
cp "Sonic The Hedgehog Spinball (USA, Europe).zip" sega-gamegear/.
cp "Tails Adventures (World) (En,Ja).zip" sega-gamegear/.