mkdir gameboy
cp "Alleyway (World).zip" gameboy/.
cp "Castlevania - The Adventure (USA).zip" gameboy/.
cp "Castlevania II - Belmont's Revenge (USA, Europe).zip" gameboy/.
cp "Castlevania Legends (USA, Europe) (SGB Enhanced).zip" gameboy/.
cp "Contra - The Alien Wars (USA) (SGB Enhanced).zip" gameboy/.
cp "Donkey Kong (World) (Rev A) (SGB Enhanced).zip" gameboy/.
cp "Dr. Franken (USA).zip" gameboy/.
cp "Dr. Mario (World).zip" gameboy/.
cp "Final Fantasy Adventure (USA).zip" gameboy/.
cp "Final Fantasy Legend II (USA).zip" gameboy/.
cp "Final Fantasy Legend III (USA).zip" gameboy/.
cp "Final Fantasy Legend, The (USA).zip" gameboy/.
cp "Gargoyle's Quest (USA, Europe).zip" gameboy/.
cp "Kirby's Dream Land (USA, Europe).zip" gameboy/.
cp "Kirby's Dream Land 2 (USA, Europe) (SGB Enhanced).zip" gameboy/.
cp "Legend of Zelda, The - Link's Awakening (USA, Europe).zip" gameboy/.
cp "Legend of Zelda, The - Oracle of Ages (USA).zip" gameboy/.
cp "Legend of Zelda, The - Oracle of Seasons (USA).zip" gameboy/.
cp "Mario's Picross (USA, Europe) (SGB Enhanced).zip" gameboy/.
cp "Mega Man - Dr. Wily's Revenge (USA).zip" gameboy/.
cp "Mega Man II (USA).zip" gameboy/.
cp "Mega Man III (USA).zip" gameboy/.
cp "Mega Man IV (USA).zip" gameboy/.
cp "Metal Gear Solid (USA).zip" gameboy/.
cp "Metroid II - Return of Samus (World).zip" gameboy/.
cp "Mole Mania (USA, Europe) (SGB Enhanced).zip" gameboy/.
cp "Pocket Bomberman (Europe) (SGB Enhanced).zip" gameboy/.
cp "Pokemon Pinball (USA) (Rumble Version) (SGB Enhanced).zip" gameboy/.
cp "SolarStriker (World).zip" gameboy/.
cp "Super Mario Bros. Deluxe (USA, Europe).zip" gameboy/.
cp "Super Mario Land (World).zip" gameboy/.
cp "Super Mario Land 2 - 6 Golden Coins (USA, Europe).zip" gameboy/.
cp "Super Off Road (USA, Europe).zip" gameboy/.
cp "Super R.C. Pro-Am (USA, Europe).zip" gameboy/.
cp "Tetris (World) (Rev A).zip" gameboy/.
cp "Wario Land - Super Mario Land 3 (World).zip" gameboy/.
cp "Wario Land 3 (World) (En,Ja).zip" gameboy/.
cp "Wario Land II (USA, Europe) (SGB Enhanced).zip" gameboy/.