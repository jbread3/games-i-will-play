mkdir sega-32x
cp "After Burner Complete ~ After Burner (Japan, USA).zip" sega-32x/.
cp "Blackthorne (USA).zip" sega-32x/.
cp "Cosmic Carnage (Europe).zip" sega-32x/.
cp "Doom (Japan, USA).zip" sega-32x/.
cp "Knuckles' Chaotix (Europe).zip" sega-32x/.
cp "Kolibri (USA, Europe).zip" sega-32x/.
cp "Metal Head (Japan, USA) (En,Ja).zip" sega-32x/.
cp "Mortal Kombat II (Japan, USA).zip" sega-32x/.
cp "NBA Jam - Tournament Edition (World).zip" sega-32x/.
cp "Shadow Squadron ~ Stellar Assault (USA, Europe).zip" sega-32x/.
cp "Space Harrier (Japan, USA).zip" sega-32x/.
cp "Star Wars Arcade (USA).zip" sega-32x/.
cp "Tempo (Japan, USA).zip" sega-32x/.
cp "Virtua Fighter (Japan, USA).zip" sega-32x/.
cp "Virtua Racing Deluxe (USA).zip" sega-32x/.
cp "WWF Raw (World).zip" sega-32x/.
cp "WWF WrestleMania - The Arcade Game (USA).zip" sega-32x/.
