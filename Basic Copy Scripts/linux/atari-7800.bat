mkdir atari-7800
cp "Asteroids (USA).zip" atari-7800/.
cp "Centipede (USA).zip" atari-7800/.
cp "Choplifter! (USA).zip" atari-7800/.
cp "Dig Dug (USA).zip" atari-7800/.
cp "Donkey Kong (USA).zip" atari-7800/.
cp "Donkey Kong Junior (USA).zip" atari-7800/.
cp "Galaga (USA).zip" atari-7800/.
cp "Joust (USA).zip" atari-7800/.
cp "Mario Bros. (USA).zip" atari-7800/.
cp "Ms. Pac-Man (USA).zip" atari-7800/.
cp "Pole Position II (USA).zip" atari-7800/.
cp "Robotron - 2084 (USA).zip" atari-7800/.