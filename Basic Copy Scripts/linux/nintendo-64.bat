mkdir nintendo64
cp "007 - GoldenEye (USA).zip" nintendo64/.
cp "1080 TenEighty Snowboarding (Japan, USA) (En,Ja).zip" nintendo64/.
cp "Banjo-Kazooie (USA).zip" nintendo64/.
cp "Banjo-Tooie (USA).zip" nintendo64/.
cp "Bomberman 64 (USA).zip" nintendo64/.
cp "Carmageddon 64 (USA).zip" nintendo64/.
cp "Castlevania (USA).zip" nintendo64/.
cp "Conker's Bad Fur Day (USA).zip" nintendo64/.
cp "Cruis'n USA (USA).zip" nintendo64/.
cp "Cruis'n World (USA).zip" nintendo64/.
cp "Diddy Kong Racing (USA) (En,Fr).zip" nintendo64/.
cp "Donkey Kong 64 (USA).zip" nintendo64/.
cp "Doom 64 (USA).zip" nintendo64/.
cp "Excitebike 64 (USA).zip" nintendo64/.
cp "F-1 World Grand Prix (USA).zip" nintendo64/.
cp "F-Zero X (USA).zip" nintendo64/.
cp "Jet Force Gemini (USA).zip" nintendo64/.
cp "Kirby 64 - The Crystal Shards (USA).zip" nintendo64/.
cp "Legend of Zelda, The - Majora's Mask (USA).zip" nintendo64/.
cp "Legend of Zelda, The - Ocarina of Time (USA).zip" nintendo64/.
cp "Madden NFL 2002 (USA).zip" nintendo64/.
cp "Mario Golf (USA).zip" nintendo64/.
cp "Mario Kart 64 (USA).zip" nintendo64/.
cp "Mario Party (USA).zip" nintendo64/.
cp "Mario Party 2 (USA).zip" nintendo64/.
cp "Mario Party 3 (USA).zip" nintendo64/.
cp "NBA Hangtime (USA).zip" nintendo64/.
cp "Ogre Battle 64 - Person of Lordly Caliber (USA).zip" nintendo64/.
cp "Paper Mario (USA).zip" nintendo64/.
cp "Paperboy (USA).zip" nintendo64/.
cp "Perfect Dark (USA).zip" nintendo64/.
cp "Pilotwings 64 (USA).zip" nintendo64/.
cp "Star Fox 64 (USA).zip" nintendo64/.
cp "Super Mario 64 (USA).zip" nintendo64/.
cp "Super Smash Bros. (USA).zip" nintendo64/.
cp "Tony Hawk's Pro Skater (USA).zip" nintendo64/.
cp "Tony Hawk's Pro Skater 2 (USA).zip" nintendo64/.
cp "Tony Hawk's Pro Skater 3 (USA).zip" nintendo64/.
cp "Top Gear Overdrive (USA).zip" nintendo64/.
cp "Top Gear Rally 2 (USA).zip" nintendo64/.
cp "Turok - Dinosaur Hunter (USA).zip" nintendo64/.
cp "Turok 2 - Seeds of Evil (USA).zip" nintendo64/.
cp "Wave Race 64 (USA).zip" nintendo64/.
cp "WCW vs. nWo - World Tour (USA).zip" nintendo64/.
cp "Wipeout 64 (USA).zip" nintendo64/.
cp "WWF No Mercy (USA).zip" nintendo64/.
cp "Yoshi's Story (USA) (En,Ja).zip" nintendo64/.