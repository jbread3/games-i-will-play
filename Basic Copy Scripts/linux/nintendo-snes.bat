mkdir snes
cp "ActRaiser (USA).zip" snes/.
cp "ActRaiser 2 (USA).zip" snes/.
cp "Aero Fighters (USA).zip" snes/.
cp "Aerobiz (USA).zip" snes/.
cp "Aladdin (USA).zip" snes/.
cp "B.O.B. (USA).zip" snes/.
cp "Battletoads in Battlemaniacs (USA).zip" snes/.
cp "Battletoads-Double Dragon (USA).zip" snes/.
cp "Biker Mice from Mars (USA).zip" snes/.
cp "Blackthorne (USA).zip" snes/.
cp "Bonkers (USA).zip" snes/.
cp "Boogerman - A Pick and Flick Adventure (USA).zip" snes/.
cp "Breath of Fire (USA).zip" snes/.
cp "Breath of Fire II (USA).zip" snes/.
cp "Bubsy in - Claws Encounters of the Furred Kind (USA).zip" snes/.
cp "Bugs Bunny - Rabbit Rampage (USA).zip" snes/.
cp "Bust-A-Move (USA).zip" snes/.
cp "Castlevania - Dracula X (USA).zip" snes/.
cp "Choplifter III - Rescue-Survive (USA).zip" snes/.
cp "Chrono Trigger (USA).zip" snes/.
cp "Clay Fighter (USA).zip" snes/.
cp "Claymates (USA).zip" snes/.
cp "Contra III - The Alien Wars (USA).zip" snes/.
cp "Cool Spot (USA).zip" snes/.
cp "Cool World (USA).zip" snes/.
cp "Daffy Duck - The Marvin Missions (USA).zip" snes/.
cp "Darius Twin (USA).zip" snes/.
cp "Demon's Crest (USA).zip" snes/.
cp "Donkey Kong Country (USA) (Rev 2).zip" snes/.
cp "Donkey Kong Country 2 - Diddy's Kong Quest (USA) (En,Fr) (Rev 1).zip" snes/.
cp "Donkey Kong Country 3 - Dixie Kong's Double Trouble! (USA) (En,Fr).zip" snes/.
cp "Doom (USA).zip" snes/.
cp "Doom Troopers - Mutant Chronicles (USA).zip" snes/.
cp "Dragon View (USA).zip" snes/.
cp "Dragon's Lair (USA).zip" snes/.
cp "Duel, The - Test Drive II (USA).zip" snes/.
cp "EarthBound (USA).zip" snes/.
cp "Earthworm Jim (USA).zip" snes/.
cp "Earthworm Jim 2 (USA).zip" snes/.
cp "F-Zero (USA).zip" snes/.
cp "Final Fantasy - Mystic Quest (USA) (Rev 1).zip" snes/.
cp "Final Fantasy II (USA) (Rev 1).zip" snes/.
cp "Final Fantasy III (USA) (Rev 1).zip" snes/.
cp "Final Fight (USA).zip" snes/.
cp "Final Fight 2 (USA).zip" snes/.
cp "Final Fight 3 (USA).zip" snes/.
cp "Final Fight Guy (USA).zip" snes/.
cp "Frogger (USA).zip" snes/.
cp "Gemfire (USA).zip" snes/.
cp "Goof Troop (USA).zip" snes/.
cp "Gradius III (USA).zip" snes/.
cp "GunForce (USA).zip" snes/.
cp "Harvest Moon (USA).zip" snes/.
cp "Illusion of Gaia (USA).zip" snes/.
cp "Jeopardy! - Deluxe Edition (USA).zip" snes/.
cp "Jimmy Connors Pro Tennis Tour (USA).zip" snes/.
cp "Joe & Mac (USA).zip" snes/.
cp "Joe & Mac 2 - Lost in the Tropics (USA).zip" snes/.
cp "Judge Dredd (USA).zip" snes/.
cp "Jungle Book, The (USA).zip" snes/.
cp "Jungle Strike (USA).zip" snes/.
cp "Jurassic Park (USA) (Rev 1).zip" snes/.
cp "Jurassic Park Part 2 - The Chaos Continues (USA) (En,Fr,De,It).zip" snes/.
cp "Ken Griffey Jr. Presents Major League Baseball (USA) (Rev 1).zip" snes/.
cp "Killer Instinct (USA) (Rev 1).zip" snes/.
cp "Kirby's Dream Course (USA).zip" snes/.
cp "Kirby's Dream Land 3 (USA).zip" snes/.
cp "Krusty's Super Fun House (USA) (Rev 1).zip" snes/.
cp "Legend of the Mystical Ninja, The (USA).zip" snes/.
cp "Legend of Zelda, The - A Link to the Past (USA).zip" snes/.
cp "Lemmings (USA) (Rev 1).zip" snes/.
cp "Lion King, The (USA).zip" snes/.
cp "Lost Vikings 2 (USA).zip" snes/.
cp "Lost Vikings, The (USA).zip" snes/.
cp "Lufia & the Fortress of Doom (USA).zip" snes/.
cp "Lufia II - Rise of the Sinistrals (USA).zip" snes/.
cp "Madden NFL '94 (USA).zip" snes/.
cp "Magical Quest Starring Mickey Mouse, The (USA).zip" snes/.
cp "Marvel Super Heroes in War of the Gems (USA).zip" snes/.
cp "Mega Man 7 (USA).zip" snes/.
cp "Mega Man X (USA) (Rev 1).zip" snes/.
cp "Mega Man X2 (USA).zip" snes/.
cp "Mega Man X3 (USA).zip" snes/.
cp "Mortal Kombat (USA).zip" snes/.
cp "Mortal Kombat 3 (USA).zip" snes/.
cp "Mortal Kombat II (USA) (Rev 1).zip" snes/.
cp "Mr. Do! (USA).zip" snes/.
cp "Mr. Nutz (USA) (En,Fr).zip" snes/.
cp "NBA Jam (USA) (Rev 1).zip" snes/.
cp "NBA Jam - Tournament Edition (USA).zip" snes/.
cp "New Horizons (USA).zip" snes/.
cp "NHL '94 (USA).zip" snes/.
cp "Ninja Gaiden Trilogy (USA).zip" snes/.
cp "Ogre Battle - The March of the Black Queen (USA).zip" snes/.
cp "Operation Thunderbolt (USA).zip" snes/.
cp "Out of this World (USA).zip" snes/.
cp "Paperboy 2 (USA).zip" snes/.
cp "Phalanx (USA).zip" snes/.
cp "Pilotwings (USA).zip" snes/.
cp "R-Type III (USA).zip" snes/.
cp "Raiden Trad (USA).zip" snes/.
cp "Rock n' Roll Racing (USA).zip" snes/.
cp "Secret of Evermore (USA).zip" snes/.
cp "Secret of Mana (USA).zip" snes/.
cp "Shaq-Fu (USA).zip" snes/.
cp "Side Pocket (USA).zip" snes/.
cp "SimCity (USA).zip" snes/.
cp "Simpsons, The - Bart's Nightmare (USA).zip" snes/.
cp "Skyblazer (USA).zip" snes/.
cp "Soul Blazer (USA).zip" snes/.
cp "Sparkster (USA).zip" snes/.
cp "Spider-Man - Venom - Maximum Carnage (USA).zip" snes/.
cp "Star Fox (USA) (Rev 2).zip" snes/.
cp "Street Fighter Alpha 2 (USA).zip" snes/.
cp "Street Fighter II (USA).zip" snes/.
cp "Street Fighter II Turbo (USA) (Rev 1).zip" snes/.
cp "Stunt Race FX (USA) (Rev 1).zip" snes/.
cp "Sunset Riders (USA).zip" snes/.
cp "Super Adventure Island (USA).zip" snes/.
cp "Super Adventure Island II (USA).zip" snes/.
cp "Super Bomberman (USA).zip" snes/.
cp "Super Bomberman 2 (USA).zip" snes/.
cp "Super Bonk (USA).zip" snes/.
cp "Super Castlevania IV (USA).zip" snes/.
cp "Super Chase H.Q. (USA).zip" snes/.
cp "Super Double Dragon (USA).zip" snes/.
cp "Super Ghouls'n Ghosts (USA).zip" snes/.
cp "Super Mario All-Stars + Super Mario World (USA).zip" snes/.
cp "Super Mario Kart (USA).zip" snes/.
cp "Super Mario RPG - Legend of the Seven Stars (USA).zip" snes/.
cp "Super Mario World (USA).zip" snes/.
cp "Super Mario World 2 - Yoshi's Island (USA).zip" snes/.
cp "Super Metroid (Japan, USA) (En,Ja).zip" snes/.
cp "Super Nova (USA).zip" snes/.
cp "Super Punch-Out!! (USA).zip" snes/.
cp "Super R-Type (USA).zip" snes/.
cp "Super Smash T.V. (USA).zip" snes/.
cp "Super Star Wars - Return of the Jedi (USA) (Rev 1).zip" snes/.
cp "Super Star Wars - The Empire Strikes Back (USA) (Rev 1).zip" snes/.
cp "Super Street Fighter II (USA).zip" snes/.
cp "Super Turrican (USA).zip" snes/.
cp "Super Turrican 2 (USA).zip" snes/.
cp "Tecmo Super Bowl (USA).zip" snes/.
cp "Tecmo Super Bowl II - Special Edition (USA).zip" snes/.
cp "Teenage Mutant Ninja Turtles - Tournament Fighters (USA).zip" snes/.
cp "Teenage Mutant Ninja Turtles IV - Turtles in Time (USA).zip" snes/.
cp "Tiny Toon Adventures - Buster Busts Loose! (USA).zip" snes/.
cp "Top Gear (USA).zip" snes/.
cp "Top Gear 2 (USA).zip" snes/.
cp "Top Gear 3000 (USA).zip" snes/.
cp "U.N. Squadron (USA).zip" snes/.
cp "Ultimate Mortal Kombat 3 (USA).zip" snes/.
cp "Uniracers (USA).zip" snes/.
cp "Vegas Stakes (USA).zip" snes/.
cp "Wheel of Fortune (USA).zip" snes/.
cp "Wild Guns (USA).zip" snes/.
cp "Wings 2 - Aces High (USA).zip" snes/.
cp "Wolfenstein 3-D (USA).zip" snes/.
cp "WWF Raw (USA).zip" snes/.
cp "WWF Royal Rumble (USA).zip" snes/.
cp "WWF Super WrestleMania (USA).zip" snes/.
cp "X-Men - Mutant Apocalypse (USA).zip" snes/.
cp "Ys III - Wanderers from Ys (USA).zip" snes/.
cp "Zombies Ate My Neighbors (USA).zip" snes/.