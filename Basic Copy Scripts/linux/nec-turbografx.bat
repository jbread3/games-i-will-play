mkdir nec-turbografx16
cp "Aero Blasters (USA).zip" nec-turbografx16/.
cp "Air Zonk (USA).zip" nec-turbografx16/.
cp "Alien Crush (USA).zip" nec-turbografx16/.
cp "Ballistix (USA).zip" nec-turbografx16/.
cp "Blazing Lazers (USA).zip" nec-turbografx16/.
cp "Bomberman '93 (USA).zip" nec-turbografx16/.
cp "Bomberman (USA).zip" nec-turbografx16/.
cp "Bonk III - Bonk's Big Adventure (USA).zip" nec-turbografx16/.
cp "Bonk's Adventure (USA).zip" nec-turbografx16/.
cp "Bonk's Revenge (USA).zip" nec-turbografx16/.
cp "Bravoman (USA).zip" nec-turbografx16/.
cp "Darkwing Duck (USA).zip" nec-turbografx16/.
cp "Devil's Crush (USA).zip" nec-turbografx16/.
cp "Fantasy Zone (USA).zip" nec-turbografx16/.
cp "Final Lap Twin (USA).zip" nec-turbografx16/.
cp "Galaga '90 (USA).zip" nec-turbografx16/.
cp "J.J. & Jeff (USA).zip" nec-turbografx16/.
cp "Keith Courage in Alpha Zones (USA).zip" nec-turbografx16/.
cp "King of Casino (USA).zip" nec-turbografx16/.
cp "Legendary Axe II, The (USA).zip" nec-turbografx16/.
cp "Legendary Axe, The (USA).zip" nec-turbografx16/.
cp "Military Madness (USA).zip" nec-turbografx16/.
cp "Neutopia (USA).zip" nec-turbografx16/.
cp "New Adventure Island (USA).zip" nec-turbografx16/.
cp "Ninja Spirit (USA).zip" nec-turbografx16/.
cp "R-Type (USA).zip" nec-turbografx16/.
cp "Raiden (USA).zip" nec-turbografx16/.
cp "SideArms - Hyper Dyne (USA).zip" nec-turbografx16/.
cp "Splatterhouse (USA).zip" nec-turbografx16/.
cp "Street Fighter II' - Champion Edition (Japan).zip" nec-turbografx16/.
cp "Super Star Soldier (USA).zip" nec-turbografx16/.
cp "Super Volleyball (USA).zip" nec-turbografx16/.
cp "Yo, Bro (USA).zip" nec-turbografx16/.