# games-i-will-play

A series of batch files that operate on various no-intro sets to build a smaller set of games I *may* actually play.

## Systems I may actually play
* Nintendo Entertainment System (NES/Famicom)
* Nintendo Gameboy 
* Nintendo Gameboy Color
* Super Nintendo Entertainment System (SNES/Super Famicom)
* Nintendo Gameboy Advance
* Nintendo 64
* Sega Genesis (Mega Drive)
* Sega 32X
* Sega Saturn
* Sega Dreamcast
* NEC TurboGrafx-16 (PC Engine)
* Atari 2600
* Atari 7800
* Atari Jaguar

## Systems I don't play
* Atari Lynx
* Microsoft Xbox
* Microsoft Xbox 360
* Microsoft Xbox One
* Sony Playstation
* Sony Playstation 2
* Sony Playstation Portable
* Sony Playstation 3
* Sony Playstation Vita
* Sony Playstation 4
* Atari XE Video Game System (XEGS)
* Atari 5200
* NEC TurboExpress
* NEC PC Engine SuperGrafx
* NEC TurboDuo
* Sega Gamegear
* Sega Mastersystem
* Nintendo Gamecube
* Nintendo Wii
* Nintendo Wii-U
* Nintendo Switch
* Nintendo Virtual Boy
