ECHO off
REM Loop through every file in this directory and see if its hash matches known good hash
mkdir good
for %%f in (*.*) do (
    CALL :GetMD5Hash %%f value2
)
Exit /B %ERRORLEVEL%

:GetMD5Hash
    REM I'm not a fan of this line.
    SETLOCAL enabledelayedexpansion
    
    SET /a count = 1
    FOR /f "skip=1 delims=:" %%a IN ('CertUtil -hashfile %~1 MD5') DO (
        IF !count! equ 1 SET "md5=%%a"
        SET /a count+=1
    )
    REM Clean up the md5
    SET "md5=%md5: =%
    REM echo %~1 : %md5%
    CALL :FindMD5InList %~1 %md5%
EXIT /B 0

:FindMD5InList
    REM find any lines in known MD5 list that match the file hash
    FOR /f "tokens=1" %%c IN ('findstr %~2  knownMd5List.txt') DO (
        REM If there is output then a match was found
        REM Copy the file to output directory
        echo %~1
        copy %~1 good\.
    )
EXIT /B 0